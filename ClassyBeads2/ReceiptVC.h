//
//  ReceiptVC.h
//  ClassyBeads2
//
//  Created by Pavan Kataria on 09/03/2014.
//  Copyright (c) 2014 Pavan Kataria. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OrderLineDataModel.h"
#import "ReceiptCell.h"
#import "OrderTotalFooterVC.h"
#import "SBJson.h"
#import "ASIHTTPRequest.h"
#import "ASIFormDataRequest.h"
#import "OrderLineDataModel.h"
#import "CustomerDataModel.h"

@interface ReceiptVC : UIViewController<UITableViewDelegate, UITableViewDataSource, UIScrollViewDelegate, UIAlertViewDelegate>{
    OrderTotalFooterVC *orderTotalFooterView;
    UIBarButtonItem *buttonProcessOrder;

}
-(id) initInEditModeWithOrderObject:(OrdersDataModel*)orderObject;
-(id) initWithCustomerObject:(CustomerDataModel*)customerObject andInvoiceDataArray:(NSMutableArray*)invoiceDataArray;

@property (nonatomic, retain) NSMutableArray *invoiceDataArray;
@property (nonatomic, weak) IBOutlet UITableView *invoiceTable;
@property (retain, nonatomic) ASIHTTPRequest *serverRequest;
@property (nonatomic, retain) CustomerDataModel *customerDetails;
@property (nonatomic, retain) OrdersDataModel *editModeOrderObject;

@end
