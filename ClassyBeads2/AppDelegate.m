//
//  AppDelegate.m
//  ClassyBeads2
//
//  Created by Pavan Kataria on 25/01/2014.
//  Copyright (c) 2014 Pavan Kataria. All rights reserved.
//

#import "AppDelegate.h"
#import <CoreData/CoreData.h>


@implementation AppDelegate

@synthesize sideMenuViewController = _sideMenuViewController;
@synthesize mainViewController = _mainViewController;
@synthesize menuViewController = _menuViewController;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];

    [self.window makeKeyAndVisible];

    // Override point for customization after application launch.
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    [self showSplitViewController];
    
    
    
    
    
    
    
    return YES;
}

//
//- (void)initializeCoreDataStack {
//    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"ClassyBeads" withExtension:@"sqlite"];
//    ZAssert(modelURL, @"Failed to find model URL");
//    
//    NSManagedObjectModel *mom = nil;
//    mom = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL]; ZAssert(mom, @"Failed to initialize model");
//    
//    
//    
//    
//    
//                                  
//                                  
//    
//    
//    dispatch_queue_t queue = NULL;
//    queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
//    dispatch_async(queue, ^{
//        NSFileManager *fileManager = [NSFileManager defaultManager];
//        NSArray *directoryArray = [fileManager URLsForDirectory:NSDocumentDirectory
//                                                      inDomains:NSUserDomainMask];
//        NSURL *storeURL = nil;
//        storeURL = [directoryArray lastObject];
//        storeURL = [storeURL URLByAppendingPathComponent:@"Classybeads.sqlite"];
//        NSError *error = nil;
//        NSPersistentStore *store = nil;
//        store = [self.persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType
//                                  configuration:nil
//                                            URL:storeURL
//                                        options:nil
//                                          error:&error];
//        if (!store) {
//            ALog(@"Error adding persistent store to coordinator %@\n%@",
//                 [error localizedDescription], [error userInfo]);
//            //Present a user facing error
//            ￼￼￼
//        }
//}
-(void)showSplitViewController{

    _mainViewController = [[MainVC7 alloc] initWithNibName:nil bundle:nil];
    _menuViewController = [[MenuVC7 alloc] initWithNibName:nil bundle:nil];
    
    // create a new side menu
    //If its a factory user logging in then
    _sideMenuViewController = [[TWTSideMenuViewController alloc] initWithMenuViewController:_menuViewController
                                                                         mainViewController:[[PKNavigationController alloc] initWithRootViewController:_mainViewController]];

    // specify the shadow color to use behind the main view controller when it is scaled down.
    _sideMenuViewController.shadowColor = [UIColor blackColor];
    
    // specify a UIOffset to offset the open position of the menu
    _sideMenuViewController.edgeOffset = UIOffsetMake(0.0f, 0.0f);
    
    // specify a scale to zoom the interface — the scale is 0.0 (scaled to 0% of it's size) to 1.0 (not scaled at all). The example here specifies that it zooms so that the main view is 56.34% of it's size in open mode.
    _sideMenuViewController.zoomScale = 0.8034f;
    
    // set the side menu controller as the root view controller
    self.window.rootViewController = _sideMenuViewController;
    _menuViewController.managedObjectContext = self.managedObjectContext;

    [_sideMenuViewController ForceCorrection];
//    [_menuViewController openOrderScreen];
    [_sideMenuViewController openMenuAnimated:YES completion:nil];
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

//Explicitly write Core Data accessors
- (NSManagedObjectContext *) managedObjectContext {
    if (managedObjectContext != nil) {
        return managedObjectContext;
    }
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        managedObjectContext = [[NSManagedObjectContext alloc] init];
        [managedObjectContext setPersistentStoreCoordinator: coordinator];
    }
    
    return managedObjectContext;
}

- (NSManagedObjectModel *)managedObjectModel {
    if (managedObjectModel != nil) {
        return managedObjectModel;
    }
    managedObjectModel = [NSManagedObjectModel mergedModelFromBundles:nil];
    
    return managedObjectModel;
}
- (BOOL)progressivelyMigrateURL:(NSURL*)sourceStoreURL
                         ofType:(NSString*)type
                        toModel:(NSManagedObjectModel*)finalModel
                          error:(NSError**)error
{
    
    
    /**
     In this code segment, we first retrieve the metadata from the source URL. If that metadata is not nil, we ask the final model whether the metadata is compatible with it. If it is, we are happy and done. We then set the error pointer to nil and return YES. If it isn’t compatible, we need to try to figure out the mapping model and potentially the interim data model to migrate to.
     
     */
    NSDictionary *sourceMetadata =
    [NSPersistentStoreCoordinator metadataForPersistentStoreOfType:type
                                                               URL:sourceStoreURL
     
                                                             error:error];
    if (!sourceMetadata){
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        [dict setValue:@"Source meta data not found" forKey:NSLocalizedDescriptionKey];
        //Populate the error
        *error = [NSError errorWithDomain:@"Zarra" code:8001 userInfo:dict];

        return NO;
    }
    if ([finalModel isConfiguration:nil
        compatibleWithStoreMetadata:sourceMetadata]) {
        *error = nil;
        return YES;
    }
    else{
        // Migration is needed
        NSLog(@"\n#######################\n####################### MIGRATION IS NEEDED!!!!\n######################\n\n");
//        return YES;
    }
    
    
    /**
     To proceed to the next step in the migration, we need to find all the managed object models in the bundle and loop through them. The goal at this point is to get all the models and figure out which one we can migrate to. Since these models will probably be in their own bundles, we have to first look for the bundles and then look inside each of them.
     
     
     In this code block, we first grab all the resource paths from the mainBundle that are of type momd. This gives us a list of all the model bundles. We then loop through the list and look for mom resources inside each and add them to an overall array. Once that’s done, we look inside the mainBundle again for any freestanding mom resources. Finally, we do a failure check to make sure we have some models to look through. If we can’t find any, we populate the NSError and return NO.
     */
    
    //Find the source model
    NSManagedObjectModel *sourceModel = [NSManagedObjectModel mergedModelFromBundles:nil
                                                                    forStoreMetadata:sourceMetadata];
    
    NSAssert(sourceModel != nil, ([NSString stringWithFormat:@"Failed to find source model\n%@", sourceMetadata]));
    
    //Find all of the mom and momd files in the Resources directory
    NSMutableArray *modelPaths = [NSMutableArray array];
    NSArray *momdArray = [[NSBundle mainBundle] pathsForResourcesOfType:@"momd"
                                                            inDirectory:nil];
    for (NSString *momdPath in momdArray) {
        NSString *resourceSubpath = [momdPath lastPathComponent]; NSArray *array = [[NSBundle mainBundle]
                                                                                    pathsForResourcesOfType:@"mom"
                                                                                    inDirectory:resourceSubpath];
        [modelPaths addObjectsFromArray:array];
    }
    
    NSArray* otherModels = [[NSBundle mainBundle] pathsForResourcesOfType:@"mom"
                                                              inDirectory:nil];
    [modelPaths addObjectsFromArray:otherModels];
    
    if (!modelPaths || ![modelPaths count]) {
        //Throw an error if there are no models
        NSMutableDictionary *dict = [NSMutableDictionary dictionary]; [dict setValue:@"No models found in bundle"
                                                                              forKey:NSLocalizedDescriptionKey];
        //Populate the error
        *error = [NSError errorWithDomain:@"Zarra" code:8001 userInfo:dict];
        
        return NO;
    }
    
    
    
    
    
    
    /*
     Now the complicated part comes in. Since it is not currently possible to get an NSMappingModel with just the source model and then determine the destina- tion model, we have to instead loop through every model we find, instantiate it, plug it in as a possible destination, and see whether there is a mapping model in existence. If there isn’t, we continue to the next one.
     
    */
    NSMappingModel *mappingModel = nil;
    NSManagedObjectModel *targetModel = nil;
    NSString *modelPath = nil;
    
    for(modelPath in modelPaths){
        targetModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:[NSURL fileURLWithPath:modelPath]];
        mappingModel = [NSMappingModel mappingModelFromBundles:nil
                                                forSourceModel:sourceModel
                                              destinationModel:targetModel];
        
        //If we found a mapping model then proceed
        if(mappingModel) break;
        //Release the target model and keep looking
        targetModel = nil;
    }
    
    //We have tested every model, if nil here we failed
    if(!mappingModel){
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        [dict setValue:@"No models found in bundle" forKey:NSLocalizedDescriptionKey];
        *error = [NSError errorWithDomain:@"Zarra" code:8001 userInfo:dict];
        
        return NO;
    }
    
    
    /*
     This above section is probably the most complicated piece of the progressive migration routine. In this section, we’re looping through all the models that were previously discovered. For each of those models, we’re instantiating the model and then asking NSMappingModel for an instance that will map between our known source model and the current model. If we find a mapping model, we break from our loop and continue. Otherwise, we release the instantiated model and continue the loop. After the loop, if the mapping model is still nil, we generate an error stating that we cannot discover the progression between the source model and the target and return NO. At this point, we should have all the components we need for one migration. The source model, target model, and mapping model are all known quantities. Now it’s time to migrate!
     */
    
    
    /*
     
     Performing the Migration
     In this block, we are instantiating an NSMigrationManager (if we needed something special, we would build our own manager) with the source model and the desti- nation model. We are also building up a unique path to migrate to. In this example, we are using the destination model’s filename as the unique change to the source store’s path. Once the destination path is built, we then tell the migration manager to perform the migration and check to see whether it was successful. If it wasn’t, we simply return NO because the NSError will be populated by the NSMigrationManager. If it’s successful, there are only three things left to do: move the source out of the way, replace it with the new destination store, and finally recurse.
     
     */
    
    NSMigrationManager *manager = [[NSMigrationManager alloc]
                                   initWithSourceModel:sourceModel
                                   destinationModel:targetModel];
    
    NSString *modelName = [[modelPath lastPathComponent]
                           stringByDeletingPathExtension];
    
    NSString *storeExtension = [[sourceStoreURL path] pathExtension];
    NSString *storePath = [[sourceStoreURL path] stringByDeletingPathExtension];
    //Build a path to write the new store
    storePath = [NSString stringWithFormat:@"%@.%@.%@", storePath, modelName, storeExtension];
    NSURL *destinationStoreURL = [NSURL fileURLWithPath:storePath];
    
    if (![manager migrateStoreFromURL:sourceStoreURL
                                 type:type
                              options:nil
                     withMappingModel:mappingModel
                     toDestinationURL:destinationStoreURL
                      destinationType:type
                   destinationOptions:nil
                                error:error]) {
        
        
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        [dict setValue:[NSString stringWithFormat:@"Couldn't migrate store from: \nSource:%@ \nto: \nDestination%@\nwith the following mapping model's entity mappings: %@", sourceStoreURL, destinationStoreURL, [mappingModel entityMappingsByName]] forKey:NSLocalizedDescriptionKey];
        //Populate the error
        *error = [NSError errorWithDomain:@"Zarra" code:8001 userInfo:dict];
        return NO;
    }
    
    /*
     In this final code block, we first create a permanent location for the original store to be moved to. In this case, we will use a globally unique string gener- ated from the NSProcessInfo class and attach the destination model’s filename and the store’s extension to it. Once that path is built, we move the source to it and then replace the source with the destination. At this point, we are at the same spot we were when we began except that we are now one version closer to the current model version.
     */
    
    
    /*
     Now we need to loop back to step 1 again in our workflow. Therefore, we will recursively call ourselves, returning the result of that recurse. As you can recall from the beginning of this method, if we are now at the current version, we will simply return YES, which will end the recursion.
     */
    
    NSString *guid = [[NSProcessInfo processInfo] globallyUniqueString];
    guid = [guid stringByAppendingPathExtension:modelName];
    guid = [guid stringByAppendingPathExtension:storeExtension];
    NSString *appSupportPath = [storePath stringByDeletingLastPathComponent];
    NSString *backupPath = [appSupportPath stringByAppendingPathComponent:guid];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if (![fileManager moveItemAtPath:[sourceStoreURL path]
                              toPath:backupPath
                               error:error]) {
        //Failed to copy the file
        
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        [dict setValue:@"Failed to copy the sourceStoreUrl to the backup path url" forKey:NSLocalizedDescriptionKey];
        *error = [NSError errorWithDomain:@"Zarra" code:8001 userInfo:dict];

        return NO;
    }
  
    
    //Move the destination to the source path
    if (![fileManager moveItemAtPath:storePath toPath:[sourceStoreURL path]
                               error:error]) {
        //Try to back out the source move first, no point in checking it for errors
        [fileManager moveItemAtPath:backupPath
                             toPath:[sourceStoreURL path]
                              error:nil];
        
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        [dict setValue:@"failed to move the destination to the source path" forKey:NSLocalizedDescriptionKey];
        *error = [NSError errorWithDomain:@"Zarra" code:8001 userInfo:dict];

        return NO;
    }
    else{
        //Source store was moved to the backup path which can be deleted.
        [fileManager removeItemAtPath:backupPath error:error];
        
        NSLog(@"Backup path %@ to be deleted", backupPath);

    }
    
    
    
    
    
    //We may not be at the "current" model yet, so recurse
    return [self progressivelyMigrateURL:sourceStoreURL ofType:type
                                 toModel:finalModel
                                   error:error];
    
}


- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
    if (persistentStoreCoordinator != nil) {
        return persistentStoreCoordinator;
    }
    NSURL *storeURL = [NSURL fileURLWithPath: [[self applicationDocumentsDirectory]
                                               stringByAppendingPathComponent: @"ClassyBeads.sqlite"]];
    NSLog(@"STORE URL: %@", storeURL);
    
    
    NSError *error = nil;
    persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc]
                                  initWithManagedObjectModel:[self managedObjectModel]];
    
//    NSDictionary *options = @{
//                              NSMigratePersistentStoresAutomaticallyOption : @YES,
//                              NSInferMappingModelAutomaticallyOption : @YES
//                              };
    

//    NSDictionary *sourceMetadata = [NSPersistentStoreCoordinator metadataForPersistentStoreOfType:NSSQLiteStoreType URL:storeURL error:&error];
    
    
    
    // perform core data migrations if necessary
    if(![self progressivelyMigrateURL:storeURL ofType:NSSQLiteStoreType toModel:[self managedObjectModel] error:&error])
    {
        // reset the persistent store on fail
        NSString *documentDir = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents"];
        NSError *error = nil;
        [[NSFileManager defaultManager] removeItemAtPath:[NSBundle pathForResource:@"ClassyBeads" ofType:@"sqlite" inDirectory:documentDir] error:&error];
        
        NSLog(@"ERROR: %@", [error localizedDescription]);
    }
    else
    {
        NSLog(@"migration succeeded!");
    }
//    NSManagedObjectModel *destinationModel = [persistentStoreCoordinator managedObjectModel];
//    BOOL pscCompatible = [destinationModel isConfiguration:nil compatibleWithStoreMetadata:sourceMetadata];
//    
//    
//    
//    if(!pscCompatible) {
//        // Migration is needed
//        NSLog(@"\n#######################\n####################### MIGRATION IS NEEDED!!!!\n######################\n\n");
//        
//        
//    }
    
    if (![persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil
                                                             URL:storeURL options:nil error:&error]) {
//        NSLog(@"4");
        /*Error for     store creation should be handled in here*/
        ALog(@"Error adding persistent store to coordinator %@\n%@",
             [error localizedDescription], [error userInfo]);
        NSString *msg = nil;
        
        msg = [NSString stringWithFormat:@"The products database %@%@%@\n%@\n%@",
               @"is either corrupt or was created by a newer ", @"version of Classy Beads. Please contact ", @"support to assist with this error.",
               [error localizedDescription], [error userInfo]];
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error"
                                                            message:msg
                                                           delegate:self
                                                  cancelButtonTitle:@"Quit"
                                                  otherButtonTitles:nil];
        [alertView show];
        return nil;
    }
    
    NSLog(@"end of persistent store method");
    return persistentStoreCoordinator;
}

- (NSString *)applicationDocumentsDirectory {
    return [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
}

/* (...Existing Application Code...) */

- (void)dealloc {
//    [managedObjectContext release];
//    [managedObjectModel release];
//    [persistentStoreCoordinator release];
//    
    /* (...Existing Dealloc Releases...) */
}



+(AppDelegate *)getAppDelegateReference{
    
    return ((AppDelegate *)[[UIApplication sharedApplication] delegate]);
}

@end
