//
//  AddCustomerVC.h
//  ClassyBeads2
//
//  Created by Pavan Kataria on 15/04/2014.
//  Copyright (c) 2014 Pavan Kataria. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PkPlaceHolderTextField.h"
@class AddCustomerVC;

@protocol PKAddCustomerDelegate <NSObject>
@required
-(void)addedCustomerVCWithCustomer:(CustomerDataModel*)customer;
-(void)editedCustomerVCWithCustomer:(CustomerDataModel*)customer;
-(void)deletedCustomer:(CustomerDataModel*)customer;
@end

@interface AddCustomerVC : UIViewController<UITextFieldDelegate, UIAlertViewDelegate>{
    UITextField * memberName;
    UITextField * memberPhone;
    UITextField * memberEmail;

    __weak id <PKAddCustomerDelegate> delegate;
    CustomerDataModel *customer;
}

//@property (nonatomic, retain) IBOutlet UITextField * memberName;
//@property (nonatomic, retain) IBOutlet UITextField * memberPhone;
//@property (nonatomic, retain) IBOutlet UITextField * memberEmail;

@property (nonatomic, retain) IBOutlet UIButton * customerActionButton;
@property (nonatomic, retain) IBOutlet UIButton * cancelButton;
@property (retain, nonatomic) ASIHTTPRequest *serverRequest;

@property (weak) id<PKAddCustomerDelegate> delegate;


-(id)initWithCustomerObject:(CustomerDataModel*)customerObject;
@end
