//
//  OrdersEditTC.m
//  ClassyBeads2
//
//  Created by Pavan Kataria on 08/04/2014.
//  Copyright (c) 2014 Pavan Kataria. All rights reserved.
//

#import "OrdersEditTC.h"

@interface OrdersEditTC ()

@end

@implementation OrdersEditTC
@synthesize serverRequest;



- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
//    [self retrieveCustomers];
    
    [self setNavigationButtons];
//    [self setupExtraVariables];
//    [self retrieveCustomers];
}

-(void)setNavigationButtons{
    UIBarButtonItem *showButton = [[UIBarButtonItem alloc] initWithTitle:@"Menu" style:UIBarButtonItemStylePlain target:self action:@selector(show:)];
    [self.navigationItem setLeftBarButtonItem:showButton animated:YES];
    self.title = @"Members";
    
}

-(void)show:(id)sender{
    //    [[[AppDelegate getAppDelegateReference] splitViewController] toggleMasterView:sender];
    [self.sideMenuViewController openMenuAnimated:YES completion:nil];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


//-(void)retrieveCustomers{
//    [self retrieveWithRequestStringType:GetOrdersRequest withDictionary:nil];
//}
//-(void)retrieveWithRequestStringType:(NSString*)typeOfRequest withDictionary:(NSMutableDictionary*)inputdictionary{
//    self.loading = TRUE;
//    
//    NSLog(@"loading = TRUE");
//    NSLog(@"Retrieve %@ method called", typeOfRequest);
//    NSString *urlString = [NSString stringWithFormat:@"%@/Secure/CB/%@", @"http://www.riabp.com/CB", typeOfRequest];
//    NSString *encodedUrlString = [urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
//    NSURL *url = [[NSURL alloc] initWithString:encodedUrlString];
//    serverRequest = nil;
//    serverRequest = [ASIFormDataRequest requestWithURL:url];
//    [serverRequest addRequestHeader:@"Content-Type" value:@"application/json"];
//    [serverRequest addRequestHeader:@"Request-Method" value:@"POST"];
//    
//    if([typeOfRequest isEqualToString:GetOrdersRequest]){
//        [serverRequest setUserInfo:[NSDictionary dictionaryWithObject:GetOrdersRequest forKey:@"RequestType"]];
//    }
//    [serverRequest setDelegate:self];
//    [serverRequest setDidFinishSelector:@selector(requestSucceeded:)];
//    [serverRequest setDidFailSelector:@selector(requestFailed:)];
//    [serverRequest startAsynchronous];
//    
//}
//
//-(void)requestSucceeded:(ASIHTTPRequest*)request{
//    self.loading = FALSE;
//    
//    //some weird table header view offset shit was happening and it happened when setting the refreshcontrol after the retrieve agents request was made.
//    
//    if(![self.customerTableRefreshControl.attributedTitle isEqualToAttributedString:[self getRefreshControlAttributedStringDefaultWithString:@"Refresh Data"]]){
//        [self setRefreshControlToDefault];
//    }
//    NSLog(@"loading = false");
//    NSInteger statusCode = [[[request responseHeaders] objectForKey:@"StatusCode"] intValue];
//    NSLog(@"StatusCode: %@", [[request responseHeaders] objectForKey:@"StatusCode"]);
//    
//    NSString *myString = [[NSString alloc] initWithData:[request responseData] encoding:NSUTF8StringEncoding];
//    NSDictionary *JSONDictionary = [myString JSONValue];
//    
//    switch (statusCode) {
//        case 400:
//        case 401:
//        {
//            NSLog(@"display error message");
//            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:[[request.responseString JSONValue] objectForKey:@"Message"] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
//            [alert show];
//            
//            break;
//        }
//        case 200:
//            NSLog(@"status code = 200 so successful");
//            
//            //statesRetrieval succeeded
//            if([[[request userInfo] objectForKey:@"RequestType"] isEqualToString:GetOrdersRequest]){
////                NSLog(@"\n\nJSON RESULT: %@\n\n", JSONDictionary);
////                [self setupTableWithDictionary:[JSONDictionary objectForKey:@"orders"] withRequest:GetOrdersRequest];
//                [self setupOrderTableWithDictionary:[JSONDictionary objectForKey:@"orders"]];
//                [self.customerTableRefreshControl endRefreshing];
//                
//            }
//            break;
//        default:
//            NSLog(@"went to none: %d or %@", statusCode, [[request responseHeaders] objectForKey:@"StatusCode"]);
//            break;
//    }
//}
//-(void)requestFailed:(ASIHTTPRequest*)request{
//    //statesRetrieval failed
//    [self setRefreshControlToDefault];
//    self.loading = FALSE;
//    NSLog(@"loading = false");
//    if([[[request userInfo] objectForKey:@"RquestType"] isEqualToString:GetOrdersRequest]){
//        NSLog(@"retrieving states failed so trying again");
//        [self retrieveCustomers];
//    }
//}
//
//
//
//-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
//    NSLog(@"row: %d", indexPath.row);
//    if([self.dataSetFilteredArray count] == 0){
//        static NSString *cellID = @"loadingcell";
//        
//        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
//        if (!cell) {
//            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
//            cell.selectionStyle = UITableViewCellSelectionStyleNone;
//            
//        }
//        if(indexPath.row == 3){
//            cell.textLabel.text = [NSString stringWithFormat:@"Loading Orders"];
//            [cell.textLabel setBackgroundColor:[UIColor clearColor]];
//                
//            //[cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
//        }
//        return cell;
//    }
//    else{
//        static NSString *fellID = @"productcell";
//        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:fellID];
//        
//        OrdersDataModel *odm = [self.dataSetFilteredArray objectAtIndex:indexPath.row];
//        
//        if (!cell) {
//            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:fellID];
//            
//            [cell.textLabel setFont:[UIFont fontWithName:@"CartoGothicStd-Bold" size:23.0]];
//            [cell.textLabel setTextColor:[UIColor blackColor]];
//            cell.textLabel.adjustsFontSizeToFitWidth = YES;
//            [cell.textLabel setHighlightedTextColor:APP_COLOR_WHITE];
//            cell.selectedBackgroundView = [PreferencesConfig7 getDefaultSelectedBackgroundView];
//            [cell setBackgroundColor:[UIColor whiteColor]];
//        }
//        [cell.textLabel setText:odm.orderReferenceNumber];
//        return cell;
//    }
//
//}









@end
