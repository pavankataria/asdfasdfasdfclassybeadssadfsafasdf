//
//  CustomerOrderSelectionTC.m
//  ClassyBeads2
//
//  Created by Pavan Kataria on 08/04/2014.
//  Copyright (c) 2014 Pavan Kataria. All rights reserved.
//

#import "CustomerOrderSelectionTC.h"

@interface CustomerOrderSelectionTC ()

@end

@implementation CustomerOrderSelectionTC
@synthesize serverRequest;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    [self setNavigationButtons];
    self.title = @"Customer selection";
    [self retrieveCustomers];
}
-(void)setNavigationButtons{
    UIBarButtonItem *showButton = [[UIBarButtonItem alloc] initWithTitle:@"Menu" style:UIBarButtonItemStylePlain target:self action:@selector(show:)];
    [self.navigationItem setLeftBarButtonItem:showButton animated:YES];
    self.title = @"Customer Selection";
    
    
}
-(void)show:(id)sender{
    [self.sideMenuViewController openMenuAnimated:YES completion:nil];
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source



#pragma mark - ASIHTTPRequest methods

-(void)retrieveCustomers{
    [self retrieveWithRequestStringType:GetCustomersRequest2 withDictionary:nil];
}
-(void)retrieveWithRequestStringType:(NSString*)typeOfRequest withDictionary:(NSMutableDictionary*)inputdictionary{
    self.loading = TRUE;
    
    NSLog(@"loading = TRUE");
    NSLog(@"Retrieve %@ method called", typeOfRequest);
    NSString *urlString = [NSString stringWithFormat:@"%@/Secure/CB/%@", @"http://www.riabp.com/CB", typeOfRequest];
    NSString *encodedUrlString = [urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSURL *url = [[NSURL alloc] initWithString:encodedUrlString];
    serverRequest = nil;
    serverRequest = [ASIFormDataRequest requestWithURL:url];
    [serverRequest addRequestHeader:@"Content-Type" value:@"application/json"];
    [serverRequest addRequestHeader:@"Request-Method" value:@"POST"];
    
    if([typeOfRequest isEqualToString:GetCustomersRequest2]){
        [serverRequest setUserInfo:[NSDictionary dictionaryWithObject:GetCustomersRequest2 forKey:@"RequestType"]];
    }
    [serverRequest setDelegate:self];
    [serverRequest setDidFinishSelector:@selector(requestSucceeded:)];
    [serverRequest setDidFailSelector:@selector(requestFailed:)];
    [serverRequest startAsynchronous];
    
}

-(void)requestSucceeded:(ASIHTTPRequest*)request{
    self.loading = FALSE;
    
    //some weird table header view offset shit was happening and it happened when setting the refreshcontrol after the retrieve agents request was made.
    
    if(![self.customerTableRefreshControl.attributedTitle isEqualToAttributedString:[self getRefreshControlAttributedStringDefaultWithString:@"Refresh Data"]]){
        [self setRefreshControlToDefault];
    }
    NSLog(@"loading = false");
    NSInteger statusCode = [[[request responseHeaders] objectForKey:@"StatusCode"] intValue];
    NSLog(@"StatusCode: %@", [[request responseHeaders] objectForKey:@"StatusCode"]);
    
    NSString *myString = [[NSString alloc] initWithData:[request responseData] encoding:NSUTF8StringEncoding];
    NSDictionary *JSONDictionary = [myString JSONValue];
    
    switch (statusCode) {
        case 400:
        case 401:
        {
            NSLog(@"display error message");
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:[[request.responseString JSONValue] objectForKey:@"Message"] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
            [alert show];
            
            break;
        }
        case 200:
            NSLog(@"status code = 200 so successful");
            
            //statesRetrieval succeeded
            if([[[request userInfo] objectForKey:@"RequestType"] isEqualToString:GetCustomersRequest2]){
                //NSLog(@"\n\nJSON RESULT: %@\n\n", JSONDictionary);
                [self setupTableWithDictionary:[JSONDictionary objectForKey:@"customers"] withRequest:GetCustomersRequest2];
                [self.customerTableRefreshControl endRefreshing];
                
            }
            break;
        default:
            NSLog(@"went to none: %d or %@", statusCode, [[request responseHeaders] objectForKey:@"StatusCode"]);
            break;
    }
}
-(void)requestFailed:(ASIHTTPRequest*)request{
    //statesRetrieval failed
    [self setRefreshControlToDefault];
    self.loading = FALSE;
    NSLog(@"loading = false");
    if([[[request userInfo] objectForKey:@"RquestType"] isEqualToString:GetCustomersRequest2]){
        NSLog(@"retrieving states failed so trying again");
        [self retrieveCustomers];
    }
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    //    NSLog(@"f");
    if([self.originalDataSetArray count] > 0){
        //        [tableView deselectRowAtIndexPath:indexPath animated:YES];
        CustomerDataModel *customerDM;
        //        if([[[AppDelegate getAppDelegateReference] splitViewController] isShowingMaster]){
        //            [[[AppDelegate getAppDelegateReference] splitViewController] toggleMasterView:nil];
        //        };
        if (tableView == self.searchDisplayController.searchResultsTableView) {
            customerDM = [self.dataSetFilteredArray objectAtIndex:indexPath.row];
        }
        else{
            customerDM  = [[self.customerTableIndices valueForKey:[[[self.customerTableIndices allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] objectAtIndex:indexPath.section]] objectAtIndex:indexPath.row];
        }
        //        NSLog(@"selected the following agent) name: %@, state: %@", agentDM.customerName, agentDM.custm);
        
        
        
        //        if(setupForAgentAccounts){
        //            NSLog(@"setup agent accounts");
        //            AgentAccountsVC *agentAccountsVC = [[AgentAccountsVC alloc] initWithNibName:@"AgentAccountsVC" bundle:nil];
        //            agentAccountsVC.agentDM = agentDM;
        //            [(PKNavigationController*)([[[AppDelegate getAppDelegateReference] splitViewController] detailViewController]) pushViewController:agentAccountsVC animated:YES];
        //
        //        }
        //        else if(setupForAgentBoxDiscounts){
        //            NSLog(@"setup product box discounts");
        //            ProductBoxDiscountVC *agentProductsBoxDiscounts = [[ProductBoxDiscountVC alloc] initWithNibName:@"ProductBoxDiscountVC" bundle:nil];
        //            agentProductsBoxDiscounts.agentDM = agentDM;
        //            [(PKNavigationController*)([[[AppDelegate getAppDelegateReference] splitViewController] detailViewController]) pushViewController:agentProductsBoxDiscounts animated:YES];
        //        }
        //        else{
        NSLog(@"setup take order");
        
        TakeOrderVC *takeOrderVC = [[TakeOrderVC alloc] initWithCustomerObject:customerDM];
//        takeOrderVC.customerDM = customerDM;
        //        se;f.sideMenuViewController
        //            [((PKNavigationController*)[self.sideMenuViewController mainViewController]) pushViewController:takeOrderVC animated:YES];
        
        [((PKNavigationController*)[self.sideMenuViewController mainViewController]) pushViewController:takeOrderVC animated:YES];
        
        //[(PKNavigationController*)([[[AppDelegate getAppDelegateReference] splitViewController] detailViewController]) pushViewController:takeOrderVC animated:YES];
    }
    
    
    
    //        [[[AppDelegate getAppDelegateReference] splitViewController] layoutSubviews];
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (tableView == self.searchDisplayController.searchResultsTableView) {
        if(self.dataSetFilteredArray.count == 0){
            //            NSLog(@"1");
            
            //            //NoResultsCell *noResultsCell = [NoResultsCell alloc] init
            static NSString *simpleTableIdentifier = @"SimpleTableCell";
            
            NoResultsCell *cell = (NoResultsCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
            if (cell == nil)
            {
                NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"NoResultsCell" owner:self options:nil];
                cell = [nib objectAtIndex:0];
            }
            if(indexPath.row == 2){
                ((UILabel *)cell.message).text = @"No Results"; // setText:@"Username"];
                
            }
            cell.backgroundColor = [UIColor clearColor];
            return cell;
        }
        else{
            //            NSLog(@"2");
            
            static NSString *cellID = @"SlideMenuCell";
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
            if (!cell) {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellID];
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                
                CustomArrowAccessoryView * accessory = [CustomArrowAccessoryView accessoryWithColor:APP_COLOR_MAIN];
                cell.accessoryView = accessory;
                
            }
            cell.textLabel.text = [(CustomerDataModel*)[self.dataSetFilteredArray objectAtIndex:indexPath.row] customerName];
            //            cell.detailTextLabel.text = [[(CustomerDataModel*)[customersFilteredArray objectAtIndex:indexPath.row] agentStateAbbreviation] uppercaseString];
            
            //            [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
            cell.backgroundColor = [UIColor clearColor];
            return cell;
        }
    }
    else {
        UITableViewCell *cell;
        UILabel *label;
        //No data yet
        //If original source does not contain any elements then display loading cell
        if([self.currentArrayDisplayed count] == 0){
            //            NSLog(@"3");
            
            static NSString *cellID = @"loadingcell";
            
            cell = [tableView dequeueReusableCellWithIdentifier:cellID];
            if (!cell) {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                
                label = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, 200, 44)];
                //[label setBackgroundColor:[UIColor redColor]];
                [cell.contentView addSubview:label];
            }
            if(indexPath.row == 2){
                if (self.loading == FALSE) {
                    NSLog(@"loading = false");
                    cell.textLabel.text = [NSString stringWithFormat:@"No agents"];
                }
                else{
                    NSLog(@"loading = true");
                    cell.textLabel.text = [NSString stringWithFormat:@"Loading Agents"];
                }
                //[cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
            }
        }
        
        else if([self.currentArrayDisplayed count] > 0){
            //            NSLog(@"4");
            
            static NSString *cellID = @"productcell";
            cell = [tableView dequeueReusableCellWithIdentifier:cellID];
            if (!cell) {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellID];
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                
                //label = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, 200, 44)];
                //[label setBackgroundColor:[UIColor redColor]];
                //[cell.contentView addSubview:label];
            }
            CustomerDataModel *agentDM  = [[self.customerTableIndices valueForKey:[[[self.customerTableIndices allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] objectAtIndex:indexPath.section]] objectAtIndex:indexPath.row];
            
            if([PreferencesConfig7 isStringNumeric:[agentDM customerLastOrderDate]]){
                cell.textLabel.text = [NSString stringWithFormat:@"%@   (%@)", [agentDM customerName], [agentDM customerLastOrderDate]];
            }
            else{
                cell.textLabel.text = [NSString stringWithFormat:@"%@", [agentDM customerName]];
            }
            //            cell.detailTextLabel.text = [agentDM.agentStateAbbreviation uppercaseString];
            //            [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
            CustomArrowAccessoryView *accessory = [CustomArrowAccessoryView accessoryWithColor:APP_COLOR_MAIN];
            cell.accessoryView = accessory;
            
            [cell setSelectionStyle:UITableViewCellSelectionStyleBlue];
        }
        
        cell.backgroundColor = [UIColor clearColor];
        return cell;
    }
}



#pragma mark -

@end
