//
//  OrderLineDataModel.m
//  ClassyBeads2
//
//  Created by Pavan Kataria on 26/01/2014.
//  Copyright (c) 2014 Pavan Kataria. All rights reserved.
//

#import "OrderLineDataModel.h"

@implementation OrderLineDataModel
@synthesize orderLineOrderId;
@synthesize orderLineQuantity;
@synthesize orderLineCode;
@synthesize orderLinePrice;
@synthesize orderLineTotal;


-(id)init{
    if (self = [super init]) {
        [self initialiseVariables];
    }
    return self;
}
-(void)initialiseVariables{
    orderLineQuantity = [NSNumber numberWithInt:0];
    orderLineCode = @"0";
    orderLinePrice = [NSNumber numberWithInt:0];
    orderLineTotal = [NSNumber numberWithFloat:0.00];
}

-(id)initWithJSONData:(NSDictionary*)data{
    self = [super init];
    if(self){
        self.orderLineOrderId = [[data objectForKey:@"Order_Id"] integerValue];
        self.orderLineQuantity = [NSNumber numberWithInteger:[[data objectForKey:@"OrderLine_QuantitySoldATOP"] integerValue]];
        self.orderLineCode = [data objectForKey:@"OrderLine_ProductCodeATOP"];
        self.orderLinePrice = [NSNumber numberWithFloat:[[data objectForKey:@"OrderLine_PriceOfProductATOP"] floatValue]];
        self.orderLineTotal = [NSNumber numberWithFloat:[orderLinePrice floatValue] * [orderLineQuantity integerValue]];
    }
    return self;
}
-(NSString*)orderLineConvertToTableDataRow{
    return [NSString stringWithFormat:@"<tr><td>%@</td><td>x %@</td><td>%@</td><td>%@</td></tr><tr>",
     self.orderLineQuantity,
     self.orderLineCode,
     self.orderLinePrice,
     self.orderLineTotal];
}
@end
