//
//  OrderLines.m
//  ClassyBeads2
//
//  Created by Pavan Kataria on 26/01/2014.
//  Copyright (c) 2014 Pavan Kataria. All rights reserved.
//

#import "OrderLines.h"
#import "Orders.h"


@implementation OrderLines

@dynamic orderLineProductNameATOP;
@dynamic orderLinePriceATOP;
@dynamic orderLinePercentageDiscountATOP;
@dynamic orderLineQuantitySoldATOP;
@dynamic order;

@end
