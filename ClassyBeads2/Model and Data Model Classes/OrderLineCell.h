//
//  OrderLineCell.h
//  ClassyBeads2
//
//  Created by Pavan Kataria on 26/01/2014.
//  Copyright (c) 2014 Pavan Kataria. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OrderLineCell : UITableViewCell{
    IBOutlet UILabel * orderLineQuantityLabel;
    IBOutlet UILabel * orderLineCodeLabel;
    IBOutlet UILabel * orderLinePriceLabel;
    IBOutlet UILabel * orderLineTotalLabel;
}
@property (nonatomic, retain) NSString * orderLineQuantityValue;
@property (nonatomic, retain) NSString * orderLineCodeValue;
@property (nonatomic, retain) NSString * orderLinePriceValue;
@property (nonatomic, retain) NSString * orderLineTotalValue;


@end
