//
//  OrderLineCell.m
//  ClassyBeads2
//
//  Created by Pavan Kataria on 26/01/2014.
//  Copyright (c) 2014 Pavan Kataria. All rights reserved.
//

#import "OrderLineCell.h"

@implementation OrderLineCell
//@synthesize orderLineQuantity;
//@synthesize orderLineCode;
//@synthesize orderLinePrice;
//@synthesize orderLineTotal;

@synthesize orderLineQuantityValue;
@synthesize orderLineCodeValue;
@synthesize orderLinePriceValue;
@synthesize orderLineTotalValue;


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}



-(void)setOrderLineQuantityValue:(NSString *)quantityValue{
    [orderLineQuantityLabel setText:quantityValue];
}
-(void)setOrderLineCodeValue:(NSString *)codeValue{
    [orderLineCodeLabel setText:codeValue];
}
-(void)setOrderLinePriceValue:(NSString *)priceValue{
    [orderLinePriceLabel setText:priceValue];
}
-(void)setOrderLineTotalValue:(NSString *)totalValue{
    [orderLineTotalLabel setText:totalValue];
}

//- (void)setFrame:(CGRect)frame
//{
////    if ([[[UIDevice currentDevice] systemVersion] compare:@"7.0" options:NSNumericSearch] != NSOrderedAscending) {
//        // background view covers delete button on iOS 7 !?!
//        [super setFrame:CGRectMake(0, frame.origin.y, frame.size.width, frame.size.height)];
////    } else {
////        [super setFrame:frame];
////    }
//}

//
//- (void)layoutSubviews
//{
//    [super layoutSubviews];
//    
//    for (UIView *subview in self.subviews) {
//        
//        for (UIView *subview2 in subview.subviews) {
//            
//            if ([NSStringFromClass([subview2 class]) isEqualToString:@"UITableViewCellDeleteConfirmationView"]) { // move delete confirmation view
//                
//                [subview bringSubviewToFront:subview2];
//                
//            }
//        }
//    }
//}
@end
