//
//  Product.m
//  ClassyBeads2
//
//  Created by Pavan Kataria on 19/08/2014.
//  Copyright (c) 2014 Pavan Kataria. All rights reserved.
//

#import "Product.h"


@implementation Product

@dynamic productCode;
@dynamic productPrice;
@dynamic productPriceNeedsUpdating;
@dynamic lol;

@end
