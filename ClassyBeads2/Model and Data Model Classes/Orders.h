//
//  Orders.h
//  ClassyBeads2
//
//  Created by Pavan Kataria on 26/01/2014.
//  Copyright (c) 2014 Pavan Kataria. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class OrderLines;

@interface Orders : NSManagedObject

@property (nonatomic, retain) NSDate * orderCreatedDate;
@property (nonatomic, retain) NSNumber * orderTotalPercentageDiscount;
@property (nonatomic, retain) NSNumber * orderTotalDiscount;
@property (nonatomic, retain) NSNumber * orderTotalAmount;
@property (nonatomic, retain) NSSet *orderLines;
@end

@interface Orders (CoreDataGeneratedAccessors)

- (void)addOrderLinesObject:(OrderLines *)value;
- (void)removeOrderLinesObject:(OrderLines *)value;
- (void)addOrderLines:(NSSet *)values;
- (void)removeOrderLines:(NSSet *)values;

@end
