//
//  OrderLineDataModel.h
//  ClassyBeads2
//
//  Created by Pavan Kataria on 26/01/2014.
//  Copyright (c) 2014 Pavan Kataria. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OrderLineDataModel : NSObject

@property (assign) NSInteger orderLineOrderId;
@property (nonatomic, retain) NSNumber * orderLineQuantity;
@property (nonatomic, retain) NSString * orderLineCode;
@property (nonatomic, retain) NSNumber * orderLinePrice;
@property (nonatomic, retain) NSNumber * orderLineTotal;


-(id)init;
-(id)initWithJSONData:(NSDictionary*)data;
-(NSString*)orderLineConvertToTableDataRow;
@end
