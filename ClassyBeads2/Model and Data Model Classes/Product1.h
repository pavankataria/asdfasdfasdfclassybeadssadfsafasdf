//
//  Product.h
//  ClassyBeads2
//
//  Created by Pavan Kataria on 17/08/2014.
//  Copyright (c) 2014 Pavan Kataria. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Product1 : NSManagedObject

@property (nonatomic, retain) NSString * productCode;
@property (nonatomic, retain) NSNumber * productPrice;
@property (nonatomic, retain) NSDate * productPriceLastUpdated;

@end
