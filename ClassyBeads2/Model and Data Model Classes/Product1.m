//
//  Product.m
//  ClassyBeads2
//
//  Created by Pavan Kataria on 17/08/2014.
//  Copyright (c) 2014 Pavan Kataria. All rights reserved.
//

#import "Product1.h"


@implementation Product1

@dynamic productCode;
@dynamic productPrice;
@dynamic productPriceLastUpdated;

@end
