//
//  ReceiptCell.m
//  ClassyBeads2
//
//  Created by Pavan Kataria on 09/03/2014.
//  Copyright (c) 2014 Pavan Kataria. All rights reserved.
//

#import "ReceiptCell.h"

@implementation ReceiptCell
@synthesize orderLineQuantityValue;
@synthesize orderLineCodeValue;
@synthesize orderLinePriceValue;
@synthesize orderLineTotalValue;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setOrderLineQuantityValue:(NSString *)quantityValue{
    [orderLineQuantityLabel setText:quantityValue];
}
-(void)setOrderLineCodeValue:(NSString *)codeValue{
    [orderLineCodeLabel setText:codeValue];
}
-(void)setOrderLinePriceValue:(NSString *)priceValue{
    [orderLinePriceLabel setText:priceValue];
}
-(void)setOrderLineTotalValue:(NSString *)totalValue{
    [orderLineTotalLabel setText:totalValue];
}
@end
