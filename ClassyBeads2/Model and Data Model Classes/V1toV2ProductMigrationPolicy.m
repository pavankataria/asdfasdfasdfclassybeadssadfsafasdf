//
//  V1toV2ProductMigrationPolicy.m
//  ClassyBeads2
//
//  Created by Pavan Kataria on 17/08/2014.
//  Copyright (c) 2014 Pavan Kataria. All rights reserved.
//

#import "V1toV2ProductMigrationPolicy.h"

@implementation V1toV2ProductMigrationPolicy


- (BOOL)createDestinationInstancesForSourceInstance:(NSManagedObject *)sInstance entityMapping:(NSEntityMapping *)mapping manager:(NSMigrationManager *)manager error:(NSError **)error {
    
    // Create the product managed object
    
    Product *newProductInstance = [NSEntityDescription insertNewObjectForEntityForName:[mapping destinationEntityName]
                                                                inManagedObjectContext:[manager destinationContext]];
    
    
    NSString *productCode = [sInstance valueForKey:Key(Product, productCode)];
    NSNumber *productPrice = [sInstance valueForKey:Key(Product, productPrice)];
    
    NSLog(@"current source product code value: %@ and price: %@", productCode, [productPrice stringValue]);
    [newProductInstance setProductCode: productCode];
    [newProductInstance setProductPrice:productPrice];
    
    /**
     The previous old product entries didnt have anything for the last attribute,
     where as the new instances of product entity should have a detault value of YES.
     */
    [newProductInstance setProductPriceNeedsUpdating:[NSNumber numberWithBool:YES]];
    
    
    // Set up the association between the old Product and the new Product for the migration manager
    
    [manager associateSourceInstance:sInstance
             withDestinationInstance:newProductInstance
                    forEntityMapping:mapping];
    return YES;
}
@end
