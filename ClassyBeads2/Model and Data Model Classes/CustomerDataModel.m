//
//  CustomerDataModel.m
//  RajamFMCGApp
//
//  Created by Pavan Kataria on 08/04/2014.
//  Copyright (c) 2013 Pavan Kataria. All rights reserved.
//

#import "CustomerDataModel.h"

@implementation CustomerDataModel
@synthesize customerId;
@synthesize customerName;
//@synthesize customerBalance;
//@synthesize customerPercentageDiscount;
@synthesize customerOrderCount;
//@synthesize customerComments;

//@synthesize customerStateId;
//@synthesize customerStateName;
//@synthesize customerStateAbbreviation;

//@synthesize customerDirectionId;
//@synthesize customerDirectionName;
@synthesize customerLastOrderDate;
@synthesize stateSelected;

@synthesize customerPhone;
@synthesize customerEmail;

/**
 
 "customer_Balance" = 12;
 "customer_Id" = 8;
 "customer_Name" = asd;
 "customer_PercentageDiscount" = 1;
 "Direction_Id" = 3;
 "Direction_Name" = South;
 "State_AbbreviatedName" = Lo;
 "State_Id" = 2;
 "State_Name" = Londonderry;
 
*/
-(id)initCustomerAccountModelWithJSONData:(NSDictionary*)data{
    self = [super init];
    if(self){
        //        NSLog(@"initWithJSONData method called with object: %@", data);
        
        /**
         "customer_Id" = 8;
         "customer_Name" = asd;
         "customer_Balance" = 12;
         "customer_PercentageDiscount" = 1;
         "State_Id" = 2;
         "State_Name" = Londonderry;
         "State_AbbreviatedName" = Lo;
         "Direction_Id" = 3;
         "Direction_Name" = South;
         
         */
        self.customerId = [[data objectForKey:@"customer_Id"] integerValue];
        self.customerName =  [data objectForKey:@"Customer_Name"];
        
        if([self.customerName length] > 0){
            self.customerName = [self.customerName stringByReplacingCharactersInRange:NSMakeRange(0,1) withString:[[self.customerName substringToIndex:1] uppercaseString]];
        }
        self.customerPhone =  [data objectForKey:@"Customer_Number"];
        self.customerEmail =  [data objectForKey:@"Customer_Email"];
        
//        self.customerStateId = [data objectForKey:@"State_Id"];
//        self.customerStateAbbreviation = [data objectForKey:@"State_AbbreviatedName"];
//        self.customerBalance = [data objectForKey:@"customer_Balance"];
        
        NSLog(@"customer init: n: %@, p: %@, e: %@", self.customerName, self.customerPhone, self.customerEmail);
    }
    return self;

}

-(id)initWithJSONData:(NSDictionary*)data{
    self = [super init];
    if(self){
//        NSLog(@"%@", data);
        self.customerId = [[data objectForKey:@"Customer_Id"] integerValue];
        self.customerName =  [data objectForKey:@"Customer_Name"];
        
        if([self.customerName length] > 0){
            self.customerName = [self.customerName stringByReplacingCharactersInRange:NSMakeRange(0,1) withString:[[self.customerName substringToIndex:1] uppercaseString]];
        }
        
        self.customerOrderCount = [[data objectForKey:@"Customer_OrderCount"] integerValue];
        
        NSDateFormatter *inputDateFormat = [[NSDateFormatter alloc] init];
        [inputDateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];

        NSString *string = [data objectForKey:@"Customer_LastOrderDate"];
//        NSLog(@"customer data init: lastorderdate: %@", string);
        NSDate *myInputDate = [inputDateFormat dateFromString:string];

        if(myInputDate == NULL){
            self.customerLastOrderDate = @"No record";
        }
        else{
            NSDate *currentDate = [NSDate date];

            NSInteger differenceInDays = [PreferencesConfig7 daysBetweenDate:myInputDate andDate:currentDate];
            self.customerLastOrderDate  = [NSString stringWithFormat:@"%d", differenceInDays];
        }
        
        self.customerPhone =  [data objectForKey:@"Customer_Number"];
        self.customerEmail =  [data objectForKey:@"Customer_Email"];
        
        //        self.customerStateId = [data objectForKey:@"State_Id"];
        //        self.customerStateAbbreviation = [data objectForKey:@"State_AbbreviatedName"];
        //        self.customerBalance = [data objectForKey:@"customer_Balance"];
        
//        NSLog(@"customer init: n: %@, p: %@, e: %@", self.customerName, self.customerPhone, self.customerEmail);
//        NSLog(@"CUSTOMER ID = %d", (int)self.customerId);
    }
    return self;
}

@end
