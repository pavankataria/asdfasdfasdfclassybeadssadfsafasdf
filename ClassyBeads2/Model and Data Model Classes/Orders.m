//
//  Orders.m
//  ClassyBeads2
//
//  Created by Pavan Kataria on 26/01/2014.
//  Copyright (c) 2014 Pavan Kataria. All rights reserved.
//

#import "Orders.h"
#import "OrderLines.h"


@implementation Orders

@dynamic orderCreatedDate;
@dynamic orderTotalPercentageDiscount;
@dynamic orderTotalDiscount;
@dynamic orderTotalAmount;
@dynamic orderLines;

@end
