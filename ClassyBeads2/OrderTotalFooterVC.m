//
//  OrderTotalFooterVC.m
//  ClassyBeads2
//
//  Created by Pavan Kataria on 07/03/2014.
//  Copyright (c) 2014 Pavan Kataria. All rights reserved.
//

#import "OrderTotalFooterVC.h"

@interface OrderTotalFooterVC ()

@end

@implementation OrderTotalFooterVC
@synthesize totalItems;
@synthesize totalCost;


-(void)setTotalItems:(NSUInteger)total{
    [totalItemsLabel setText:[NSString stringWithFormat:@"%d items", total]];
}
-(void)setTotalCost:(CGFloat)total{
    [totalCostLabel setText:[NSString stringWithFormat:@"£%.2f", total]];

}
-(CGFloat)getTotalCost{
    NSLog(@"get total cost method called: %@", [totalCostLabel text]);
    
    NSString *cleanedString = [[[totalCostLabel text] componentsSeparatedByCharactersInSet:[[NSCharacterSet characterSetWithCharactersInString:@"0123456789."] invertedSet]] componentsJoinedByString:@""];
    
    NSLog(@"get total cost method called: %.2f", [cleanedString floatValue]);
    NSString* formattedNumber = [NSString stringWithFormat:@"%.02f", [cleanedString floatValue]];
    float floatTwoDecimalDigits = atof([formattedNumber UTF8String]);
    
    
    return floatTwoDecimalDigits;
}
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
