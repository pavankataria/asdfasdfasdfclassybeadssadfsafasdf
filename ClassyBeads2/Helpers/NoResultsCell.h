//
//  NoResultsCell.h
//  RajamFMCGApp
//
//  Created by Pavan Kataria on 30/01/2013.
//  Copyright (c) 2013 Pavan Kataria. All rights reserved.
//
#import <UIKit/UIKit.h>

@interface NoResultsCell : UITableViewCell
@property (nonatomic, strong) IBOutlet UILabel *message;

@end
