//
//  PkPlaceHolderTextField.m
//  Rajam Oorvasi
//
//  Created by Pavan Kataria on 26/12/2013.
//  Copyright (c) 2013 Pavan Kataria. All rights reserved.
//

#import "PkPlaceHolderTextField.h"

@implementation PkPlaceHolderTextField

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)drawPlaceholderInRect:(CGRect)rect {
    
    UIColor *colour = APP_COLOR_MAIN;
    if ([self.placeholder respondsToSelector:@selector(drawInRect:withAttributes:)]){
        // iOS7 and later
        NSDictionary *attributes = @{
                                        NSForegroundColorAttributeName: colour,
                                        NSFontAttributeName: self.font
                                    };
        CGRect boundingRect = [self.placeholder boundingRectWithSize:rect.size options:0
                                                          attributes:attributes
                                                             context:nil];
        [self.placeholder drawAtPoint:CGPointMake(0, (rect.size.height/2)-boundingRect.size.height/2)
                       withAttributes:attributes];  
    }
    else {
        // iOS 6
        [colour setFill];
        [self.placeholder drawInRect:rect withFont:self.font lineBreakMode:NSLineBreakByTruncatingTail alignment:self.textAlignment];
    }
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
