//
//  DisplayPadVC.h
//  ClassyBeads2
//
//  Created by Pavan Kataria on 12/02/2014.
//  Copyright (c) 2014 Pavan Kataria. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Product.h"
@interface DisplayPadVC : UIViewController<UITextFieldDelegate, UITableViewDelegate, UITableViewDataSource>{
}
@property (nonatomic, retain) UILabel *selectedLabel;


@property (nonatomic, retain) IBOutlet UILabel *quantityLabel;
@property (nonatomic, retain) IBOutlet UILabel *productLabel;
@property (nonatomic, retain) IBOutlet UILabel *priceLabel;



@property (nonatomic, retain) NSString *dPQuantityValue;
@property (nonatomic, retain) NSString *dPProductValue;
@property (nonatomic, retain) NSString *dPPriceValue;
@property (nonatomic, assign) BOOL shouldSaveCurrentTypedProductToDatabase;

-(void)setDisplayPadSelectedLabelValue:(NSString*)stringValue;
-(UILabel*)getSelectedLabel;
-(OrderTakingLabelType)getSelectedLabelName;
-(void)clearDisplays;

-(void)moveSelectionBorderToBeginning;
-(void)moveSelectionToNextFrame;
-(void)canProcessProduct;

@property (nonatomic, retain) NSManagedObjectContext *managedObjectContext;

@end
