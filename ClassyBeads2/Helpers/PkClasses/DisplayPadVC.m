//
//  DisplayPadVC.m
//  ClassyBeads2
//
//  Created by Pavan Kataria on 12/02/2014.
//  Copyright (c) 2014 Pavan Kataria. All rights reserved.
//

#import "DisplayPadVC.h"

@interface DisplayPadVC (){
    UILabel *selectionBorder;
    
    NSMutableArray *productCodes;
    UITableView *autocompleteTableView;
    NSMutableArray *autocompleteProductCodes;
//    Product * product;
}

@end

@implementation DisplayPadVC
@synthesize selectedLabel = _selectedLabel;


@synthesize quantityLabel;
@synthesize productLabel;
@synthesize priceLabel;

@synthesize dPQuantityValue;
@synthesize dPProductValue;
@synthesize dPPriceValue;
@synthesize shouldSaveCurrentTypedProductToDatabase;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    autocompleteTableView = [[UITableView alloc] initWithFrame:
                             CGRectMake(0, productLabel.frame.origin.y, self.view.frame.size.width, self.view.frame.size.height-productLabel.frame.origin.y) style:UITableViewStylePlain];
    [autocompleteTableView setBackgroundColor:[UIColor redColor]];
    autocompleteProductCodes = [[NSMutableArray alloc] init];
    productCodes = [[NSMutableArray alloc] init];
    
//    autocompleteTableView.delegate = self;
//    autocompleteTableView.dataSource = self;
    autocompleteTableView.scrollEnabled = YES;
    autocompleteTableView.hidden = YES;
    [self.view addSubview:autocompleteTableView];
    
    
    // Do any additional setup after loading the view from its nib.
    for(int i = 0; i < 3; i++){
        UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTap:)];
        switch (i) {
            case 0:
                [quantityLabel addGestureRecognizer:singleTap];
                quantityLabel.tag = 1;
                quantityLabel.userInteractionEnabled = YES;
                break;
            case 1:
                [productLabel addGestureRecognizer:singleTap];
                productLabel.tag = 2;
                productLabel.userInteractionEnabled = YES;
                break;
            case 2:
                [priceLabel addGestureRecognizer:singleTap];
                priceLabel.tag = 3;
                priceLabel.userInteractionEnabled = YES;
                break;
            default:
                break;
        }
    }
    selectionBorder = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, quantityLabel.frame.size.height)];
    [[selectionBorder layer] setBorderWidth:6];
    [[selectionBorder layer] setBorderColor:APP_COLOR_MAIN.CGColor];
    [[selectionBorder layer] setCornerRadius:5];
    [selectionBorder setBackgroundColor:[UIColor clearColor]];
    _selectedLabel = quantityLabel;
    [self.view addSubview:selectionBorder];
    
    
    [productLabel addObserver:self
                   forKeyPath:@"text"
                      options:NSKeyValueObservingOptionNew | NSKeyValueObservingOptionOld
                      context:NULL];
    
    
    
    //1
    AppDelegate* appDelegate = [UIApplication sharedApplication].delegate;
    //2
    self.managedObjectContext = appDelegate.managedObjectContext;

}
-(void)handleSingleTap:(UIGestureRecognizer*)gestureRecognizer{
    UILabel *tappedLabel = (UILabel*)gestureRecognizer.view;
    if(tappedLabel == quantityLabel){
        NSLog(@"quantityLabel");
    }
    else if(tappedLabel == productLabel){
        NSLog(@"productLabel");
    }
    else if(tappedLabel == priceLabel){
        NSLog(@"priceLabel");
    }
    [self moveSelectionBorderToFrame:tappedLabel.frame];
    _selectedLabel = tappedLabel;
}

-(void)moveSelectionBorderToFrame:(CGRect)labelFrame{
    CGRect frame = selectionBorder.frame;
    frame.origin.y = labelFrame.origin.y;
    
    [UIView animateWithDuration:0.2
                     animations:^{
                         selectionBorder.frame = frame;
                     }
                     completion:nil];
}

-(void)moveSelectionToNextFrame{
    if(_selectedLabel == quantityLabel){
        NSLog(@"currently product label selected");

        _selectedLabel = productLabel;
    }
    else if(_selectedLabel == productLabel){
        _selectedLabel = priceLabel;

    }
    else if(_selectedLabel == priceLabel){
        _selectedLabel = quantityLabel;
    }
    [self moveSelectionBorderToFrame:_selectedLabel.frame];
}
-(void)moveSelectionBorderToBeginning{
    CGRect frame = selectionBorder.frame;
    _selectedLabel = quantityLabel;

    frame.origin.y = _selectedLabel.frame.origin.y;

    [UIView animateWithDuration:0.2
                     animations:^{
                         selectionBorder.frame = frame;
                     }
                     completion:nil];

}


-(void)setDisplayPadSelectedLabelValue:(NSString*)stringValue{
//    [[self getSelectedLabel] setText:stringValue];
    NSMutableString *string = [[_selectedLabel text] mutableCopy];
//    NSLog(@"selectedLabel's text = %@", string);
    if ([string hasPrefix:@"0."]) {
//        string = [[string substringFromIndex:1] mutableCopy];
    }
    else if ([string hasPrefix:@"0"]) {
        string = [[string substringFromIndex:1] mutableCopy];
    }
    
    //This is for the cool 1 replace feature.. if the quantity has a 1 prefix and a digit is pressed, omit 1 and then append digit
    //otherwise append to digit 1
//    if(_selectedLabel == quantityLabel){
//        if([string hasPrefix:@"1"] && [string length] == 1){
//            //If the digit 1 is not pressed then delete 1 and start with that number instead.
//            if( ! [stringValue isEqualToString:@"1"]){
//                string = [[string substringFromIndex:1] mutableCopy];
//            }
//        }
//    }
    [string appendString:stringValue];
//    NSLog(@"string new value text = %@", string);

    for(int i = 0; i < 3; i++){
        switch (i) {
            case 0:
                if(_selectedLabel == quantityLabel){
                    [self setDPQuantityValue:string];
                }
                break;
            case 1:
                if(_selectedLabel == productLabel){
                    [self setDPProductValue:string];
                }
                break;
            case 2:
                if(_selectedLabel == priceLabel){
                    [self setDPPriceValue:string];
                }
                break;
            default:
                break;
        }
    }
}
-(OrderTakingLabelType)getSelectedLabelName{
    for(int i = 0; i < 3; i++){
        switch (i) {
            case 0:
                if(_selectedLabel == quantityLabel){
                    return OrderTakingLabelType_Quantity;
                }
                break;
            case 1:
                if(_selectedLabel == productLabel){
                    return OrderTakingLabelType_ProductCode;
                }
                break;
            case 2:
                if(_selectedLabel == priceLabel){
                    return OrderTakingLabelType_Price;
                }
                break;
            default:
                break;
        }
    }
    @throw [[NSException alloc] initWithName:@"PKException" reason:@"No label Name is selected/reached" userInfo:nil];

}
-(UILabel*)getSelectedLabel{
    for(int i = 0; i < 3; i++){
        switch (i) {
            case 0:
                if(_selectedLabel == quantityLabel){
                    return quantityLabel;
                }
                break;
            case 1:
                if(_selectedLabel == productLabel){
                    return productLabel;
                }
                break;
            case 2:
                if(_selectedLabel == priceLabel){
                    return priceLabel;
                }
                break;
            default:
                break;
        }
    }
    @throw [[NSException alloc] initWithName:@"PKException" reason:@"No label is selected" userInfo:nil];
}
-(void)setDPQuantityValue:(NSString *)quantityValue{
//    NSLog(@"custom quantity setter method called");
    if([quantityValue length] > 2){
        return;
    }
    [quantityLabel setText:quantityValue];
//    NSLog(@"quantity text: %@", [quantityLabel text]);
}
-(void)setDPProductValue:(NSString *)productValue{
//    NSLog(@"custom product setter method called");
    if([productValue length] > 4){
        return;
    }
    [productLabel setText:productValue];

//    NSLog(@"product text: %@", [productLabel text]);
}
-(void)setDPPriceValue:(NSString *)priceValue{
//    NSLog(@"custom price setter method called");
    
    if([priceValue rangeOfString:@"."].location != NSNotFound){
        NSString *component = [[priceValue componentsSeparatedByString: @"."] lastObject];
//        NSLog(@"lastcomponent: %@", component);
        int length = [[[priceValue componentsSeparatedByString: @"."] lastObject] length];
        if(length > 2){
            return;
        }
    }
    [priceLabel setText:priceValue];
//    NSLog(@"price text: %@", [priceLabel text]);

}
-(void)clearDisplays{
    [self setDPQuantityValue:@"0"];
    [self setDPProductValue:@"0"];
    [self setDPPriceValue:@"0"];
//    product = nil;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Autocomplete
/*
 - (BOOL)textField:(UITextField *)textField
shouldChangeCharactersInRange:(NSRange)range
replacementString:(NSString *)string {
    if(textField == productLabel){
        autocompleteTableView.hidden = NO;
        
        NSString *substring = [NSString stringWithString:textField.text];
        substring = [substring
                     stringByReplacingCharactersInRange:range withString:string];
        [self searchAutocompleteEntriesWithSubstring:substring];
        return YES;
    }
}
 */

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    
    if ([keyPath isEqualToString:@"text"]) {
        /* etc. */

//        NSLog(@"label keypath: %@ object: %@", keyPath, object);
        NSString * pCode = [((UILabel*)object) text];
        
        if([pCode length] == 4){
            Product *product = [self retrieveDetailsForProduct:pCode];
            if(product){
                NSLog(@"product found price: %@ for product: %@", [product productPrice], [product productCode]);
                
                [self setDPPriceValue:[[product productPrice] stringValue]];
                
            }
            else{
                NSLog(@"Product not found");
            }
        }
        else{
            [self setDPPriceValue:@"0"];
        }
    }
}

-(Product*)retrieveDetailsForProduct:(NSString*)pCode{
    // initializing NSFetchRequest
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    //Setting Entity to be Queried
    NSEntityDescription * entityToBeFetched = [NSEntityDescription entityForName:@"Product" inManagedObjectContext:self.managedObjectContext];
    

    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"productCode == %@",  pCode];
    [fetchRequest setFetchLimit:1];
    [fetchRequest setEntity:entityToBeFetched];
    [fetchRequest setPredicate:predicate];
    
    
    NSError* error;
    
    if ([self.managedObjectContext countForFetchRequest:fetchRequest error:&error]){
        return [[self.managedObjectContext executeFetchRequest:fetchRequest error:&error] firstObject];
    }
    else{
        return nil;
    }
    
    
    /**
     Doing it this way returns the following error:
     2014-08-16 18:51:49.540 ClassyBeads2[4273:60b] -[Product subentitiesByName]: unrecognized selector sent to instance 0xb988ee0
     
    
    // Query on managedObjectContext With Generated fetchRequest
    NSArray *dbExistingProducts = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    NSLog(@"count of dbExistingProducts: %d ", (int)[dbExistingProducts count]);
    return ([dbExistingProducts count] == 0 ? nil : [dbExistingProducts firstObject]);
     */
}
- (void)searchAutocompleteEntriesWithSubstring:(NSString *)substring {
    // Put anything that starts with this substring into the autocompleteUrls array
    // The items in this array is what will show up in the table view
    [autocompleteProductCodes removeAllObjects];
    
    for(NSString *curString in productCodes){
        NSRange substringRange = [curString rangeOfString:substring];
        
        if (substringRange.location == 0) {
            [autocompleteProductCodes addObject:curString];
        }

    }
    [autocompleteTableView reloadData];
}



-(void)canProcessProduct{
    NSLog(@"CAN PROCESS PRODUCT");
    
    // initializing NSFetchRequest
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    //Setting Entity to be Queried
    NSEntityDescription * entityToBeFetched = [NSEntityDescription entityForName:@"Product" inManagedObjectContext:self.managedObjectContext];
    
    
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"productCode == %@", [productLabel text]];
    [fetchRequest setFetchLimit:1];
    [fetchRequest setEntity:entityToBeFetched];
    [fetchRequest setPredicate:predicate];
    
    
    NSError* error;
    Product *product;
    if ([self.managedObjectContext countForFetchRequest:fetchRequest error:&error]){
        product = [[self.managedObjectContext executeFetchRequest:fetchRequest error:&error] firstObject];
    }
    else{
        product = [NSEntityDescription
                   insertNewObjectForEntityForName:@"Product"
                   inManagedObjectContext:self.managedObjectContext];
        
        [product setProductCode:[productLabel text]];
    }
    
    NSLog(@"lol 1");
    product.productPrice = [NSNumber numberWithFloat:[[priceLabel text] floatValue]];
    NSLog(@"lol 2");
    if (![self.managedObjectContext save:&error]) {
        NSLog(@"Fail to save with error: \n%@\n%@", [error localizedDescription], [error userInfo] );
    }
    else{
        NSLog(@"save successful product: %@ and updatedPrice: %@", [product productCode], [product productPrice]);
    }
    
}
@end
