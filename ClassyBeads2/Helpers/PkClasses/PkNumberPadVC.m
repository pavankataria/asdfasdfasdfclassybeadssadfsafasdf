//
//  PkNumberPadVC.m
//  ClassyBeads2
//
//  Created by Pavan Kataria on 11/02/2014.
//  Copyright (c) 2014 Pavan Kataria. All rights reserved.
//

#import "PkNumberPadVC.h"

@interface PkNumberPadVC ()

@end

@implementation PkNumberPadVC
@synthesize delegate;
-(id)init{
    self = [super initWithNibName:@"PkNumberPadVC" bundle:nil];
    if(self != nil){
    }
    return self;
}
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    for(UIButton *button in numberPadButtons){
//        NSLog(@"lol");
        [self setGlowColorForUIButton:button];
    }
}
-(IBAction)digitPressed:(UIButton *)sender{
    [self.delegate numberPadView:self digitPressed:[NSNumber numberWithInt:[[[sender titleLabel] text] intValue]]];
}
-(IBAction)returnButtonPressed:(UIButton *)sender{
    [self.delegate numberPadViewReturnButtonDidPress:self];
}
-(IBAction)pointButtonPressed:(UIButton *)sender{
    [self.delegate numberPadViewPointButtonDidPress:self];
}
-(IBAction)backspaceButtonPressed:(UIButton *)sender{
    [self.delegate numberPadViewBackspaceButtonDidPress:self];
}

-(UIButton*)setGlowColorForUIButton:(UIButton*)button{
    [button setTitleColor:APP_COLOR_WHITE forState:UIControlStateNormal];
    [button setBackgroundImage:[PreferencesConfig7 imageWithColor:APP_COLOR_MAIN] forState:UIControlStateNormal];
    
    [button setTitleColor:APP_COLOR_WHITE forState:UIControlStateHighlighted];
    [button setBackgroundImage:[PreferencesConfig7 imageWithColor:[UIColor darkGrayColor]] forState:UIControlStateHighlighted];
    [button.layer setCornerRadius:50];
    button.layer.masksToBounds = YES;
    return button;
}






- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
