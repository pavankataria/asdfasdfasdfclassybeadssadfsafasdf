//
//  MenuTitleDataModel.h
//  ClassyBeads2
//
//  Created by Pavan Kataria on 25/01/2014.
//  Copyright (c) 2014 Pavan Kataria. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MenuTitleDataModel : NSObject

@property (nonatomic, retain) NSString *mTName;
@property (nonatomic, retain) NSString *mTClassName;


@end
