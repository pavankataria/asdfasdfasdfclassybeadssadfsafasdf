//
//  AppConfig.m
//  RajamFMCGApp
//
//  Created by Pavan Kataria on 21/01/2013.
//  Copyright (c) 2013 Pavan Kataria. All rights reserved.
//

#import "AppConfig.h"



@implementation AppConfig: NSObject {
    
}

+(BOOL)validateEmail:(NSString *) email
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    BOOL isValid = [emailTest evaluateWithObject:email];
    return isValid;
}
+(NSInteger)currentIOSDeviceType{
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        if ([[UIScreen mainScreen] bounds].size.height > 480) {
            return CurrentDeviceTypeIPHONE5;
        }else{
            return CurrentDeviceTypeIPHONE4;
        }
    }
    return CurrentDeviceTypeIPAD;
}



+(NSString *) addSuffixToNumber:(int) number
{
    NSString *suffix;
    int ones = number % 10;
    int temp = floor(number/10.0);
    int tens = temp%10;
    
    if (tens ==1) {
        suffix = @"th";
    } else if (ones ==1){
        suffix = @"st";
    } else if (ones ==2){
        suffix = @"nd";
    } else if (ones ==3){
        suffix = @"rd";
    } else {
        suffix = @"th";
    }
    
    NSString *completeAsString = [NSString stringWithFormat:@"%d%@",number,suffix];
    return completeAsString;
}
@end
