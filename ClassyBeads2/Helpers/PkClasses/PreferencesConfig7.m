//
//  PreferencesConfig7.m
//  Rajam Oorvasi
//
//  Created by Pavan Kataria on 26/12/2013.
//  Copyright (c) 2013 Pavan Kataria. All rights reserved.
//

#import "UIImage+ImageEffects.h"

@implementation PreferencesConfig7
+(UIView*)getDefaultSelectedBackgroundView{
    UIView *bgview = [[UIView alloc] init];
    bgview.backgroundColor = APP_COLOR_MAIN;
    return bgview;
}
+(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    int margin = 5;
    UIFont *font = [UIFont fontWithName:@"CartoGothicStd-Bold" size:APP_DEFAULT_HEADER_FONT_SIZE];
    NSString *title = [tableView.dataSource tableView:tableView titleForHeaderInSection:section];
    CGSize titleSize = [title sizeWithAttributes:@{NSFontAttributeName: font}];
    return (margin*2+titleSize.height);
}

+(UIView*)tableView:(UITableView*)tableView viewForHeaderInSection:(NSInteger)section withFontSize:(int)fontSize
{
    int margin = APP_DEFAULT_HEADER_MARGIN_HEIGHT;
    UIFont *font = [UIFont fontWithName:@"CartoGothicStd-Bold" size:fontSize];
    NSString *title = [tableView.dataSource tableView:tableView titleForHeaderInSection:section];
    CGSize titleSize = [title sizeWithAttributes:@{NSFontAttributeName: font}];
    
    
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, tableView.sectionHeaderHeight)];
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(15, margin, tableView.bounds.size.width - 10, titleSize.height)];
    label.text = title;
    label.font = font;
    label.backgroundColor = [UIColor clearColor];
#ifdef _switch
    label.textColor = APP_COLOR_MAIN;
    [headerView setBackgroundColor:APP_COLOR_WHITE];
#else
    label.textColor = APP_COLOR_WHITE;
    [headerView setBackgroundColor:APP_COLOR_MAIN];
#endif
    [headerView addSubview:label];
    return headerView;
    
}

+(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    //     =  [[OrderLineCell alloc] init];
//    NSLog(@"number of rows in section: %d", [tableView.dataSource tableView:tableView numberOfRowsInSection:0]);
    if([tableView.dataSource tableView:tableView numberOfRowsInSection:0] >= 1){
//        NSLog(@"LOLOL");
        UILabel *qua;
        UILabel *cod;
        UILabel *pri;
        UILabel *tot;
        
        if(tableView.editing){
            qua = [[UILabel alloc] initWithFrame:CGRectMake(50, 0, 100, APP_ORDER_ITEM_HEADER_HEIGHT)];
            cod = [[UILabel alloc] initWithFrame:CGRectMake(150, 0, 100, APP_ORDER_ITEM_HEADER_HEIGHT)];
            pri = [[UILabel alloc] initWithFrame:CGRectMake(300, 0, 200, APP_ORDER_ITEM_HEADER_HEIGHT)];
            tot = [[UILabel alloc] initWithFrame:CGRectMake(500, 0, 100, APP_ORDER_ITEM_HEADER_HEIGHT)];
        }
        else{
            qua = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, 100, APP_ORDER_ITEM_HEADER_HEIGHT)];
            cod = [[UILabel alloc] initWithFrame:CGRectMake(100, 0, 100, APP_ORDER_ITEM_HEADER_HEIGHT)];
            pri = [[UILabel alloc] initWithFrame:CGRectMake(245, 0, 200, APP_ORDER_ITEM_HEADER_HEIGHT)];
            tot = [[UILabel alloc] initWithFrame:CGRectMake(440, 0, 100, APP_ORDER_ITEM_HEADER_HEIGHT)];
            
        }
        [qua setText:@"Quantity"];
        [cod setText:@"Code"];
        [pri setText:@"Product price"];
        [tot setText:@"Total"];
        
        UIView *hV = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, APP_ORDER_ITEM_HEADER_HEIGHT)];
        [hV setBackgroundColor:[UIColor whiteColor]];
        [hV addSubview:qua];
        [hV addSubview:cod];
        [hV addSubview:pri];
        [hV addSubview:tot];
        return hV;
    }
    return [[UIView alloc] initWithFrame:CGRectZero];
}

+(UIColor*)getMainBackgroundImage{
    return [UIColor colorWithPatternImage:[PreferencesConfig7 getMainBGImageWithBlur:YES]];
}

+ (UIImage *)imageWithColor:(UIColor *)color
{
    CGRect rect = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}
+(UIImage *)getMenuBGImageWithBlur{
    UIColor *tintColor = [UIColor colorWithWhite:0.21 alpha:0.4];
    
    return [[UIImage imageNamed:@"Nebula.jpg"] applyBlurWithRadius:19
                                                           tintColor:tintColor
                                               saturationDeltaFactor:1.8
                                                           maskImage:nil];
    
}
+(UIImage *)getMainBGImageWithBlur:(BOOL)addBlur{
    if(addBlur){
        UIColor *tintColor = [UIColor colorWithWhite:0.21 alpha:0.4];

        return [[UIImage imageNamed:@"universe.jpg"] applyBlurWithRadius:19
                                                               tintColor:tintColor
                                                   saturationDeltaFactor:1.8
                                                               maskImage:nil];
    }
    else{
        return [UIImage imageNamed:@"universe.jpg"];
    }
}

        

+ (UIImage*) imageWithColor:(UIColor*)color size:(CGSize)size
{
    UIGraphicsBeginImageContext(size);
    UIBezierPath* rPath = [UIBezierPath bezierPathWithRect:CGRectMake(0., 0., size.width, size.height)];
    [color setFill];
    [rPath fill];
    UIImage* image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}



+ (NSInteger)daysBetweenDate:(NSDate*)fromDateTime andDate:(NSDate*)toDateTime
{
    NSDate *fromDate;
    NSDate *toDate;
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    
    [calendar rangeOfUnit:NSDayCalendarUnit startDate:&fromDate
                 interval:NULL forDate:fromDateTime];
    [calendar rangeOfUnit:NSDayCalendarUnit startDate:&toDate
                 interval:NULL forDate:toDateTime];
    
    NSDateComponents *difference = [calendar components:NSDayCalendarUnit
                                               fromDate:fromDate toDate:toDate options:0];
    
    return [difference day];
}



+ (BOOL) isStringNumeric:(NSString*)string{
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    NSNumber *number = [formatter numberFromString:string];
    return !!number; // If the string is not numeric, number will be nil
}



+(UIColor*)redistributeRGBwithR:(CGFloat)r g:(CGFloat)g b:(CGFloat)b a:(CGFloat)a{
    
    CGFloat threshold = 255.999;
    CGFloat m = MAX(r, MAX(g, b));
    if (m <= threshold){
        return COLOR_WITH_RGB(r, g, b, a);
    }
    CGFloat total = r + g + b;
    if(total >= 3 * threshold){
        return COLOR_WITH_RGB(threshold, threshold, threshold, a);
    }
    CGFloat x = (3 * threshold - total) / (3 * m - total);
    CGFloat gray = threshold - x * m;
    CGFloat red, green, blue;
    red = gray + x *r;
    green = gray + x * g;
    blue = gray + x * b;

//    NSLog(@"R: %f, G:%f, B:%f", red, green, blue);
    return COLOR_WITH_RGB(red, green, blue, a);
}

+(BOOL) NSStringIsValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = YES; // Discussion http://blog.logichigh.com/2010/09/02/validating-an-e-mail-address/
    NSString *stricterFilterString = @"[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}";
    NSString *laxString = @".+@([A-Za-z0-9]+\\.)+[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}



@end
