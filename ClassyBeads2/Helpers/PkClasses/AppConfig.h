//
//  AppConfig.h
//  RajamFMCGApp
//
//  Created by Pavan Kataria on 21/01/2013.
//  Copyright (c) 2013 Pavan Kataria. All rights reserved.
//

#import <AssetsLibrary/AssetsLibrary.h>

#import <Foundation/Foundation.h>

#define PK_TESTING NO
#define ALPHA                   @"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
#define NUMERIC                 @"1234567890"
#define ALPHA_NUMERIC           ALPHA NUMERIC
#define ALPHA_NUMERIC_WITHSPACE           ALPHA NUMERIC @" @"


//#define DEMO_URL2 @"http://ec2-54-251-74-199.ap-southeast-1.compute.amazonaws.com/DEMO"
//#define LIVE_URL2 @"http://ec2-54-251-74-199.ap-southeast-1.compute.amazonaws.com"
//#define WEB_SERVIVES_ACTIVE_URL2 @"none"//DEMO_URL


#define APP_NAME @"appName_OORVASI"
#define COLOR(r,g,b,a)  [UIColor colorWithRed:r green:g blue:b alpha:a]
#define COLOR_WITH_RGB(r,g,b,a)  [UIColor colorWithRed:(r/255.0) green:(g/255.0) blue:(b/255.0) alpha:(a/255.0)]
#define TESTING_URL @"http://riabp.com/LIVE/"

#define GetCustomersRequest @"Get/Customers"
#define GetCustomersRequest2 @"Get/Customers"
#define GetOrdersRequest @"Get/Orders"

#define ORDER_ITEM_ROW_HEIGHT 80
#define APP_ORDER_ITEM_HEADER_HEIGHT 40
enum{
    CurrentDeviceTypeIPAD = 0,
    CurrentDeviceTypeIPHONE4 = 1,
    CurrentDeviceTypeIPHONE5 = 2
}CurrentDeviceType;

typedef enum {
    OrderTakingLabelType_Quantity  = 0,
    OrderTakingLabelType_ProductCode = 1,
    OrderTakingLabelType_Price = 2
} OrderTakingLabelType;

@interface AppConfig : NSObject{
    
}

+(NSInteger) currentIOSDeviceType;
+(BOOL) validateEmail:(NSString *) email;
+(NSString *) addSuffixToNumber:(int)number;



#ifdef DEBUG
#define Key(class, key)              ([(class *)nil key] ? @#key : @#key)
#define ProtocolKey(protocol, key)   ([(id <protocol>)nil key] ? @#key : @#key)
#else
#define Key(class, key)              @#key
#define ProtocolKey(class, key)      @#key
#endif

@end
