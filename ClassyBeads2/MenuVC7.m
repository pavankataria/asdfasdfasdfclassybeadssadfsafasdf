//
//  MenuVC7.m
//  Rajam Oorvasi
//
//  Created by Pavan Kataria on 28/12/2013.
//  Copyright (c) 2013 Pavan Kataria. All rights reserved.
//

#import "MenuVC7.h"

#import "MainVC7.h"
#import "AppDelegate.h"
#import <QuartzCore/QuartzCore.h>


#import "MenuTitleDataModel.h"

@interface MenuVC7 (){
    NSInteger SECTION_HEIGHT;
    NSInteger ROW_HEIGHT;
    CAGradientLayer *maskLayer;
    NSMutableArray *menuTitles;
}

@end

@implementation MenuVC7
@synthesize fetchedResultsController, managedObjectContext;

@synthesize menuTable = _menuTable;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.

    
    self.view.backgroundColor = [UIColor grayColor];

    self.backgroundImageView = [[UIImageView alloc] initWithImage:[PreferencesConfig7 getMenuBGImageWithBlur]];
    self.backgroundImageView.translatesAutoresizingMaskIntoConstraints = NO;
    
    CGRect imageViewRect = [[UIScreen mainScreen] bounds];
    imageViewRect.size.width += 589;
    self.backgroundImageView.frame = imageViewRect;
    self.backgroundImageView.contentMode = UIViewContentModeScaleAspectFit;
    [self.view addSubview:self.backgroundImageView];
    self.view.frame = imageViewRect;
    NSLog(@"background size: %@", NSStringFromCGRect(imageViewRect));
    NSDictionary *viewDictionary = @{ @"imageView" : self.backgroundImageView };
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[imageView]" options:0 metrics:nil views:viewDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[imageView]" options:0 metrics:nil views:viewDictionary]];
    
    [self setupDataSource];
    [self setupMenuList];
    
//    NSLog(@"menu view controller did load");
    
}
-(void)setupDataSource{
//    NSLog(@"1");
    
    ROW_HEIGHT = 80;
    SECTION_HEIGHT = 40;
    
    menuTitles = [[NSMutableArray alloc] init];
    MenuTitleDataModel *menuObject = [[MenuTitleDataModel alloc] init];
    [menuObject setMTName:@"Take order"];
    [menuObject setMTClassName:@"CustomerOrderSelectionTC"];
    [menuTitles addObject:menuObject];
    
    MenuTitleDataModel *menuObject2 = [[MenuTitleDataModel alloc] init];
    [menuObject2 setMTName:@"Customers"];
    [menuObject2 setMTClassName:@"CustomersEditVC"];
    [menuTitles addObject:menuObject2];
    
    MenuTitleDataModel *menuObject3 = [[MenuTitleDataModel alloc] init];
    [menuObject3 setMTName:@"Manage Orders"];
    [menuObject3 setMTClassName:@"EditOrdersVC"];
    [menuTitles addObject:menuObject3];
    
    
//    for(int i = 0; i < 20; i++){
//        MenuTitleDataModel *menuObject = [[MenuTitleDataModel alloc] init];
//        [menuObject setMTName:@"Take order"];
//        [menuObject setMTClassName:@"TakeOrderVC"];
//        [menuTitles addObject:menuObject];
//    }
//    NSLog(@"array titles: %@", menuTitles);
//    NSLog(@"2");

}

-(void)setupMenuList{
    
    int kHeightCompensate = 50;
    _menuTable = [[UITableView alloc] initWithFrame:CGRectMake(100, kHeightCompensate, 400, CGRectGetWidth([[UIApplication sharedApplication] keyWindow].bounds)-kHeightCompensate*2)];
    
    [_menuTable setBackgroundColor:[UIColor clearColor]];
    [_menuTable setDataSource:self];
    [_menuTable setDelegate:self];
    [self.view addSubview:_menuTable];
    [_menuTable setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    
    [self.menuTable setTableHeaderView:[[UIView alloc]initWithFrame:CGRectZero]];
    self.menuTable.separatorColor = [UIColor colorWithRed:0.08f green:0.08f blue:0.08f alpha:1.0f];
    
    [self.menuTable setTableFooterView:[[UIView alloc] initWithFrame:CGRectZero]];

    
    _menuTable.layer.mask = [self setMaskForTable];
    [_menuTable setContentInset:UIEdgeInsetsMake(70,0,100,0)];
}
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [CATransaction begin];
    [CATransaction setDisableActions:YES];
    maskLayer.position = CGPointMake(0, scrollView.contentOffset.y);
    [CATransaction commit];
}

-(CAGradientLayer*)setMaskForTable{
    
    maskLayer = [CAGradientLayer layer];
    CGColorRef outerColor = [UIColor colorWithWhite:0.0 alpha:0.0].CGColor;
    CGColorRef innerColor = [UIColor colorWithWhite:0.0 alpha:1.0].CGColor;
    maskLayer.colors = [NSArray arrayWithObjects:(__bridge id)outerColor,
                        (__bridge id)innerColor, (__bridge id)innerColor, (__bridge id)outerColor, nil];
    maskLayer.locations = [NSArray arrayWithObjects:[NSNumber numberWithFloat:0.0],
                           [NSNumber numberWithFloat:0.2],
                           [NSNumber numberWithFloat:0.8],
                           [NSNumber numberWithFloat:1.0], nil];
    maskLayer.bounds = CGRectMake(0, 0,
                                  _menuTable.frame.size.width,
                                  _menuTable.frame.size.height);
    maskLayer.anchorPoint = CGPointZero;
    return maskLayer;
}
-(void)openOrderScreen{
    UIViewController *nextViewController = [[NSClassFromString(@"TakeOrderVC") alloc] init];
    if (nextViewController) {
        [self setMainViewWithVC:nextViewController];
    }

}

#pragma mark - UITableView Delegate methods
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return ROW_HEIGHT;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    MenuTitleDataModel *mtDM = [menuTitles objectAtIndex:indexPath.row];
    id nextViewController = [[NSClassFromString(mtDM.mTClassName) alloc] init];
    
    NSLog(@"%@", nextViewController);
    // If we got a new view controller, push it .
    
    
    if (nextViewController) {
        [self setMainViewWithVC:nextViewController];
    }
    NSLog(@"index: %d Title %@ selected", (int)indexPath.row, [mtDM mTName]);
}
-(void)setMainViewWithVC:(id)viewController{
    NSLog(@"YES RESPONDS TO CANCEL REQUEST 1");
    
//    if([[[(PKNavigationController*)[self.sideMenuViewController mainViewController] viewControllers] objectAtIndex:0] respondsToSelector:@selector(cancelRequest)]){
//        
//        NSLog(@"YES RESPONDS TO CANCEL REQUEST 2");
//        [[[(PKNavigationController*)[self.sideMenuViewController mainViewController] viewControllers] objectAtIndex:0] cancelRequest];
//    }
    
    NSLog(@"%@ and %@", NSStringFromClass([[[((PKNavigationController*)[self.sideMenuViewController mainViewController]) viewControllers] firstObject] class]), NSStringFromClass([viewController class]));
    
    if( ! [NSStringFromClass([[[((PKNavigationController*)[self.sideMenuViewController mainViewController]) viewControllers] firstObject] class]) isEqualToString:NSStringFromClass([viewController class])]){
        PKNavigationController *navController = nil;
        navController = [[PKNavigationController alloc] initWithRootViewController:viewController];
        [self.sideMenuViewController setMainViewController:navController animated:YES closeMenu:YES];
    }
    else{
        NSLog(@"Has already been opened");
        [self.sideMenuViewController closeMenuAnimated:YES completion:nil];
    }
}



#pragma mark - UITableView Datasource methods
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [menuTitles count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *MyIdentifier = @"menuTitleCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
    if(cell == nil){
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:MyIdentifier];
        cell.backgroundColor = [UIColor clearColor];
        [cell.textLabel setFrame:CGRectMake(40, 2, 350, ROW_HEIGHT)];
        cell.textLabel.textColor = APP_COLOR_MAIN;
        cell.backgroundColor = [UIColor clearColor];
        [cell.textLabel setHighlightedTextColor:APP_COLOR_WHITE];
        
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 1, 1)];
        [view setBackgroundColor:[UIColor clearColor]];
        cell.selectedBackgroundView = view;
        
//        if(userType == RajamUserTypeCEO){
            cell.textLabel.font = [UIFont fontWithName:@"CartoGothicStd-Book" size:22.0];//[UIFont fontWithName:@"Helvetica-Medium" size:22];
//        }
//        else{
//            cell.textLabel.font = [UIFont fontWithName:@"CartoGothicStd-Bold" size:21];
//        }

    }
    [cell.textLabel setTextColor:APP_COLOR_WHITE];

    cell.textLabel.text = [[menuTitles objectAtIndex:indexPath.row] mTName];
    return cell;
}





- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
