//
//  OrderTotalFooterVC.h
//  ClassyBeads2
//
//  Created by Pavan Kataria on 07/03/2014.
//  Copyright (c) 2014 Pavan Kataria. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OrderTotalFooterVC : UIViewController{
    IBOutlet UILabel *totalItemsLabel;
    IBOutlet UILabel *totalCostLabel;
}

@property (nonatomic, assign) NSUInteger totalItems;
@property (nonatomic, assign) CGFloat totalCost;
-(CGFloat)getTotalCost;
@end