//
//  TakeOrderVC.m
//  ClassyBeads2
//
//  Created by Pavan Kataria on 26/01/2014.
//  Copyright (c) 2014 Pavan Kataria. All rights reserved.
//

#import "TakeOrderVC.h"
#import "OrderLineCell.h"
#import "OrderLineDataModel.h"
#import "SelectedBackgroundView.h"
#import <AudioToolbox/AudioToolbox.h>

@interface TakeOrderVC (){
    NSMutableArray *orderLinesArray;
    NSInteger selectedLabelTag;

    NSMutableString *inputString;
    NSMutableString *rowOneText;
    NSMutableString *rowTwoText;
    NSMutableString *rowThreeText;
    NSMutableString *rowFourText;
    
    NSMutableArray *rowsText;
    
    
    BOOL shouldDisplayData;
    CGRect originalTableFrame;
    
    OrdersDataModel *editModeOrderObject;
    
    BOOL EDIT_MODE;
    
    UIAlertView *goBackToOrderList;
    UIAlertView *goBackToCustomerSelectionScreenAlert;
}
@end

@implementation TakeOrderVC
@synthesize orderLineTable = _orderLineTable;
@synthesize customerDM = _customerDM;


-(id)initWithCustomerObject:(CustomerDataModel*)customerObject{
    self = [super init];
    if( !self ) return nil;
    _customerDM = customerObject;
    return self;

}
-(id)initInOrderEditModeWithOrderObject:(OrdersDataModel*)orderObject{
    self = [super init];
    if( !self ) return nil;
    
    NSLog(@"edit order mode view controller init method called");
    editModeOrderObject = orderObject;
    NSLog(@"EDIT MODE ORDERLINE DETAILS: %@", editModeOrderObject.orderLinesDataArray);
    EDIT_MODE = YES;
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self settingUpView];
    
    
    //1
    AppDelegate* appDelegate = [UIApplication sharedApplication].delegate;
    //2
    self.managedObjectContext = appDelegate.managedObjectContext;

    
    
    
    
    
}


- (IBAction)addProductCode:(NSString *)productCode productPrice:(CGFloat)productPrice{
    
    // Add Entry to PhoneBook Data base and reset all fields
    //  1
    
    Product * newEntry = [NSEntityDescription insertNewObjectForEntityForName:@"Product"
                                                      inManagedObjectContext:self.managedObjectContext];
    //  2
    newEntry.productCode = productCode;
    newEntry.productPrice = [NSNumber numberWithFloat:productPrice];

    //  3
    NSError *error;
    if (![self.managedObjectContext save:&error]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:[NSString stringWithFormat:@"Whoops, couldn't save: %@", [error localizedDescription]] delegate:nil cancelButtonTitle:@"" otherButtonTitles:@"Before clicking ok, SCREENSHOT the error so that you can show Pavan.", nil];
        [alert show];
//        NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
    }
    
   
}



-(void)validateOrderLines{
    NSDictionary *dataValidityStatus = [self isDataInOrderLineTableValid];
    /*
     @"isDataValid"
     @"error"
     @"errorIndex"
     */
    
    if([dataValidityStatus objectForKey:@"isDataValid"]){
        [self showReceiptVC];

    }
    else{
        [SVProgressHUD showErrorWithStatus:[NSString stringWithFormat:@"Complete the %@ for item: %d", [dataValidityStatus objectForKey:@"errorMessage"], [[dataValidityStatus objectForKey:@"errorIndex"] integerValue]] maskType:SVProgressHUDMaskTypeBlack];
    }

}

-(void)settingUpView{
    [self.view setBackgroundColor:[UIColor lightGrayColor]];
    
    [self setNavigationButtons];
    
    [_orderLineTable setDelegate:self];
    [_orderLineTable setDataSource:self];
    
    NSLog(@"set editing");
    [_orderLineTable setEditing:YES];
    _orderLineTable.allowsSelectionDuringEditing = YES;
    [self setupFalseData];
    [_orderLineTable reloadData];
    [rowOne setText:@"-"];
    [rowTwo setText:@"-"];
    [rowThree setText:@"-"];
    
    
    selectedLabelTag = 0;
    
    rowsText = [[NSMutableArray alloc] init];
    rowOneText = [NSMutableString stringWithString:@"-"];
    rowTwoText = [NSMutableString stringWithString:@"-"];
    rowThreeText = [NSMutableString stringWithString:@"-"];
    rowFourText = [NSMutableString stringWithString:@"-"];
    
    
    [rowsText addObject:rowOneText];
    [rowsText addObject:rowTwoText];
    [rowsText addObject:rowThreeText];
    [rowsText addObject:rowFourText];
    
    numberPad = [[PkNumberPadVC alloc] init];
    [numberPad setDelegate:self];
    CGRect tempFrame = numberPad.view.frame;
    CGRect screenRect = self.view.frame;
    
    tempFrame.origin.x = screenRect.size.width-tempFrame.size.width - 5;
    tempFrame.origin.y = screenRect.size.height-tempFrame.size.height - 5;
    numberPad.view.frame = tempFrame;
    [self.view addSubview:numberPad.view];
    
    //#################
    //FOOTER VIEW
    //#####
    orderTotalFooterView = [[OrderTotalFooterVC alloc] init];
    tempFrame = orderTotalFooterView.view.frame;
    tempFrame.size.width = 596;
    tempFrame.size.height = ORDER_ITEM_ROW_HEIGHT;
    tempFrame.origin.y = self.view.frame.size.height-tempFrame.size.height;
    
    [orderTotalFooterView.view setFrame:tempFrame];
    
    [self.view addSubview:orderTotalFooterView.view];
    
    
    
    
    displayPad = [[DisplayPadVC alloc] init];
    //    [displayPad setDelegate:self];
    tempFrame = displayPad.view.frame;
    
    tempFrame.origin.x = screenRect.size.width-tempFrame.size.width;
    
    //Stretch the displaypad down up until the number pad. It should only do that, if there is a gap between the number and display pad.
    if(numberPad.view.frame.origin.y > (displayPad.view.frame.origin.y+displayPad.view.frame.size.height)){
        tempFrame.size.height = numberPad.view.frame.origin.y;
    }
    displayPad.view.frame = tempFrame;
    [self.view addSubview:displayPad.view];
    
    shouldDisplayData = FALSE;
    _orderLineTable.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    tempFrame = _orderLineTable.frame;
    tempFrame.size.height -= orderTotalFooterView.view.frame.size.height;
    
    originalTableFrame = _orderLineTable.frame = tempFrame;
    
    //We need the display to show straight away, so adding a row right away so that we can start adding orders
    [self hideDisplayBasedOnNumberOfRowsInTableView:_orderLineTable];
    
    
    showReceiptButton = [[UIBarButtonItem alloc] initWithTitle:@"Receipt" style:UIBarButtonItemStylePlain target:self action:@selector(showReceipt:)];
    self.navigationItem.rightBarButtonItem = showReceiptButton;
    
    if(EDIT_MODE){
        orderLinesArray = editModeOrderObject.orderLinesDataArray;
        [_orderLineTable reloadData];
        [self hideDisplayBasedOnNumberOfRowsInTableView:_orderLineTable];
        [self calculateTotalWithItems];
    }
    else{
        [self addNewRowToOrderLineTable];
        
    }
}
-(void)showReceipt:(id)sender{
    OrderLineDataModel *oldm = [orderLinesArray lastObject];
    
    //Dont press enter button if last cell is removed since enter is pressed only to ENTER the values into the last edited table row.
    //so that changes take effect incase the user accidentally forgets to press enter on the price when clicking on the receipt button
    BOOL hadToRemoveObject = NO;
    if([oldm.orderLineCode isEqualToString:@"0"]){
        [orderLinesArray removeLastObject];
        [_orderLineTable reloadData];
        hadToRemoveObject = YES;
    }
    if( ! hadToRemoveObject){
        [self numberPadViewReturnButtonDidPress:nil];
    }
    
    if([orderLinesArray count] > 0){
        [self validateOrderLines];
    }
    else{
        [SVProgressHUD showErrorWithStatus:[NSString stringWithFormat:@"Add a product first"] maskType:SVProgressHUDMaskTypeBlack];

    }
}



-(void)showReceiptVC{
    
    if(EDIT_MODE){
        ReceiptVC *receiptVC = [[ReceiptVC alloc] initInEditModeWithOrderObject:editModeOrderObject];
        [self.navigationController pushViewController:receiptVC animated:YES];

    }
    else{
        ReceiptVC *receiptVC = [[ReceiptVC alloc] initWithCustomerObject:_customerDM andInvoiceDataArray:orderLinesArray];
//        [receiptVC setCustomerDetails:_customerDM];
//        [receiptVC setInvoiceDataArray:orderLinesArray];
        [self.navigationController pushViewController:receiptVC animated:YES];
    }
    
    
    

//    [self shouldShowReceipt:YES];
}
-(UIButton*)setGlowColorForUIButton:(UIButton*)button{
    [button setTitleColor:APP_COLOR_WHITE forState:UIControlStateNormal];
    [button setBackgroundImage:[PreferencesConfig7 imageWithColor:APP_COLOR_MAIN] forState:UIControlStateNormal];
    
    [button setTitleColor:APP_COLOR_WHITE forState:UIControlStateHighlighted];
    [button setBackgroundImage:[PreferencesConfig7 imageWithColor:[UIColor darkGrayColor]] forState:UIControlStateHighlighted];
    [button.layer setCornerRadius:50];
    button.layer.masksToBounds = YES;
    return button;
}

-(void)setupFalseData{
    orderLinesArray = [[NSMutableArray alloc] init];
//    for(int i = 0; i < 6; i++){
//        OrderLineDataModel *oLDM = [[OrderLineDataModel alloc] init];
//        [orderLinesArray addObject:oLDM];
//    }
}

-(NSString*)getIndianRupeeCurrencyFormattingStringWithNumberString:(NSString*)inputNumberString{
    NSLog(@"inputString = %@", inputNumberString);
    if([inputNumberString isEqualToString:@"-"]) return inputNumberString;
    
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
    
    [numberFormatter setPositiveFormat:@"##,##,##,##,###"];
    
    NSInteger ba = [inputNumberString integerValue];
    
    return [numberFormatter stringFromNumber:[NSNumber numberWithInteger:ba]];
}
/*
-(void)numberPadKeyPressed:(id)sender{
    AudioServicesPlaySystemSound(0x450);
    NSString *character = [sender currentTitle];
    //NSLog(@"key pressed %@", character);
    //    //Backspace character pressed
    if([character isEqualToString:@"←"]){
        //NSLog(@"back space key pressed");
        NSLog(@"row text: %@, row: %d", [rowsText objectAtIndex:selectedLabelTag], selectedLabelTag);
        [[rowsText objectAtIndex:selectedLabelTag] deleteCharactersInRange:NSMakeRange([[rowsText objectAtIndex:selectedLabelTag] length]-1, 1)];
        if([[rowsText objectAtIndex:selectedLabelTag] length] == 0){
            [(NSMutableString*)[rowsText objectAtIndex:selectedLabelTag] setString:[NSMutableString stringWithString:@"-"]];
        }
    }
    else if([character isEqualToString:@"↲"]){
        CGRect frame = selectionBorder.frame;
        if(frame.origin.y >=140){
            frame.origin.y = 0;
            selectedLabelTag = 0;
        }
        else{
            frame.origin.y += 70;
            selectedLabelTag += 1;
        }
        [UIView animateWithDuration:0.2 animations:^{
            selectionBorder.frame = frame;
        }completion:nil];
        
//        if(!([[rowsText objectAtIndex:selectedLabelTag] isEqualToString:@"0"]) && !([[rowsText objectAtIndex:selectedLabelTag] isEqualToString:@"-"])){
            if([orderLinesArray count] > 0){
                NSIndexPath *selectedIndex = [_orderLineTable indexPathForSelectedRow];
                if(selectedIndex != nil){
                    NSInteger selectedCell = [selectedIndex row];
                    OrderLineDataModel *oLDM = [orderLinesArray objectAtIndex:selectedIndex.row];
//                    if(![[rowOne text] isEqualToString:@"0"] && ![[rowOne text] isEqualToString:@"-"])
                    [oLDM setOrderLineQuantity:[NSNumber numberWithInteger:[[rowOne text] integerValue]]];

//                    if(![[rowTwo text] isEqualToString:@"0"] && ![[rowTwo text] isEqualToString:@"-"])
                    [oLDM setOrderLineCode:[rowTwo text]];
                    
//                    if(![[rowThree text] isEqualToString:@"0"] && ![[rowThree text] isEqualToString:@"-"])
                    [oLDM setOrderLinePrice:[NSNumber numberWithFloat:[[rowThree text] floatValue]]];
                    
                    [oLDM setOrderLineTotal:[NSNumber numberWithFloat:([oLDM.orderLineQuantity floatValue] * [oLDM.orderLinePrice floatValue])]];
                    
                    [_orderLineTable reloadRowsAtIndexPaths:[NSArray arrayWithObject:selectedIndex] withRowAnimation:UITableViewRowAnimationFade];
                    [_orderLineTable selectRowAtIndexPath:selectedIndex animated:NO scrollPosition:UITableViewScrollPositionNone];
                    NSLog(@"The selected cell is %d", selectedCell);
                }
            }
//        }
//        else{
//        }
        
        NSLog(@"enter key pressed");
        //        return;
    }
    else{
        if([[rowsText objectAtIndex:selectedLabelTag] length] < 8){
            NSLog(@"%d key pressed", [character integerValue]);
            if([[rowsText objectAtIndex:selectedLabelTag] length] == 1 &&
               (//[[rowsText objectAtIndex:selectedLabelTag] isEqualToString:@"0"] ||
                [[rowsText objectAtIndex:selectedLabelTag] isEqualToString:@"-"])){
                   //enable anything but 0
                if(![character integerValue] == 0){
                   [[rowsText objectAtIndex:selectedLabelTag] setString:[NSMutableString stringWithString:character]];
                }
                else{
                   AudioServicesPlaySystemSound(1112); //good error sounds, goes duh duh.
                }
            }
            else{
                //check if its the quantity or product code so we can block the dot character

                if(selectedLabelTag >= 2){
                    //Check if the character is a dot
                    //dont add a dot character if there already is a dot character
                    if([character isEqualToString:@"."]){
                        if([[rowsText objectAtIndex:selectedLabelTag] rangeOfString:@"."].location == NSNotFound){
                            [[rowsText objectAtIndex:selectedLabelTag] appendString:character];
                        }
                        else{
                            AudioServicesPlaySystemSound(1112); //good error sounds, goes duh duh.
                        }
                    }
                    else{
                        [[rowsText objectAtIndex:selectedLabelTag] appendString:character];
                    }
                }
                else{
                    if([character isEqualToString:@"."]){
                        AudioServicesPlaySystemSound(1112); //good error sounds, goes duh duh.
                    }
                    else{
                        [[rowsText objectAtIndex:selectedLabelTag] appendString:character];
                    }
                }
            }
        }
    }
    NSString *finalDisplayBalance = [rowsText objectAtIndex:selectedLabelTag];
    //[self getIndianRupeeCurrencyFormattingStringWithNumberString:[rowsText objectAtIndex:selectedLabelTag]];
    switch (selectedLabelTag) {
        case 0:
            rowOne.text = finalDisplayBalance;
            break;
        case 1:
            rowTwo.text = finalDisplayBalance;
            break;
        case 2:
            rowThree.text = finalDisplayBalance;
            break;
        default:
            break;
    }
}

 */
-(void)show:(id)sender{
    //    [[[AppDelegate getAppDelegateReference] splitViewController] toggleMasterView:sender];
    [self.sideMenuViewController openMenuAnimated:YES completion:nil];
    
}

-(void)goBackToOrderList{
    NSLog(@"going back!");
    if([orderLinesArray count] > 0){
        goBackToOrderList = [[UIAlertView alloc] initWithTitle:@"Cancel order editing?" message:@"Going back will revert the order back to original, do you wish to proceed? Click Yes to revert to original order and go back" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
        [goBackToOrderList show];
    }

}
-(void)goBackToCustomerSelectionScreen{
    NSLog(@"going back!");
    if([orderLinesArray count] > 0){
        goBackToCustomerSelectionScreenAlert = [[UIAlertView alloc] initWithTitle:@"Cancel order?" message:@"Going back will delete the order, do you wish to proceed? Yes to cancel order and go back" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
        [goBackToCustomerSelectionScreenAlert show];
    }
}
-(void)setNavigationButtons{
//    UIBarButtonItem *showButton = [[UIBarButtonItem alloc] initWithTitle:@"Menu" style:UIBarButtonItemStylePlain target:self action:@selector(show:)];
//    [self.navigationItem setLeftBarButtonItem:showButton animated:YES];
    UIBarButtonItem *backButton;
    
    if(EDIT_MODE){
        self.title = [NSString stringWithFormat:@"%@ - Edit Order", editModeOrderObject.orderCustomerName];

        backButton = [[UIBarButtonItem alloc] initWithTitle:@"< Order list"
                                                                       style:UIBarButtonItemStylePlain
                                                                      target:self
                                                                      action:@selector(goBackToOrderList)];
    }
    else{
        self.title = [NSString stringWithFormat:@"%@ - Order Taking", _customerDM.customerName];

        backButton = [[UIBarButtonItem alloc] initWithTitle:@"< Customer Selection"
                                                                       style:UIBarButtonItemStylePlain
                                                                      target:self
                                                                      action:@selector(goBackToCustomerSelectionScreen)];
    }
    
    self.navigationItem.leftBarButtonItem = backButton;
//    NSArray *actionButtonItems;
//    if([[[[AppDelegate getAppDelegateReference] userInfo] objectForKey:@"User_Info"] integerValue] == 0){
//        NSLog(@"userinfo: %@", userInfo);
//        NSLog(@"a");
//        UIBarButtonItem *menuItem = [[UIBarButtonItem alloc] initWithTitle:@"Menu"
//                                                                     style:UIBarButtonItemStylePlain
//                                                                    target:self
//                                                                    action:@selector(show:)];
//        
//        UIBarButtonItem *resetItem = [[UIBarButtonItem alloc] initWithTitle:@"Reset"
//                                                                      style:UIBarButtonItemStylePlain
//                                                                     target:self
//                                                                     action:@selector(resetOrderFactoryPlanning:)];
//        UIBarButtonItem *emptyItem = [[UIBarButtonItem alloc] initWithTitle:@""
//                                                                      style:UIBarButtonItemStylePlain
//                                                                     target:nil
//                                                                     action:nil];
//        
//        actionButtonItems = @[menuItem, emptyItem, emptyItem, resetItem];
//        self.navigationItem.leftBarButtonItems = actionButtonItems;
//    }
//    else{

    
    /*
    UIBarButtonItem *menuItem = [[UIBarButtonItem alloc] initWithTitle:@"menu"
                                                                     style:UIBarButtonItemStylePlain
                                                                    target:self
                                                                        	action:@selector(show:)];
        actionButtonItems = @[menuItem];
//    }
    
    
    self.navigationItem.leftBarButtonItems = actionButtonItems;
    */
}





#pragma mark - UITableView delegate methods
//
- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	NSIndexPath *rowToSelect = indexPath;
    BOOL isEditing = tableView.editing;

    // If editing, don't allow instructions to be selected
    // Not editing: Only allow instructions to be selected
    if (isEditing && indexPath.row == [orderLinesArray count]) {
        [[_orderLineTable dataSource] tableView:_orderLineTable commitEditingStyle:UITableViewCellEditingStyleInsert forRowAtIndexPath:indexPath];
//        [tableView deselectRowAtIndexPath:indexPath animated:NO];
        rowToSelect = nil;
    }
    return rowToSelect;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    /*
     What to do on selection depends on what section the row is in.
     */
    
    if (indexPath.row < [orderLinesArray count]) {
        NSLog(@"You clicked cell number %d", indexPath.row);
        OrderLineDataModel *cOLDM = [orderLinesArray objectAtIndex:indexPath.row];
        [displayPad setDPQuantityValue:[cOLDM.orderLineQuantity stringValue]];
        [displayPad setDPProductValue:cOLDM.orderLineCode];
        [displayPad setDPPriceValue:[cOLDM.orderLinePrice stringValue]];
    }
    else{
        NSLog(@"You clicked the last cell: %d", indexPath.row);
        BOOL isEditing = tableView.editing;
        
        // If editing, don't allow instructions to be selected
        // Not editing: Only allow instructions to be selected
        if (isEditing) {
            [tableView deselectRowAtIndexPath:indexPath animated:NO];
        }
    }
}
#pragma mark - UITableView datasource methods
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return ORDER_ITEM_ROW_HEIGHT;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
//    return [orderLinesArray count];
//    NSLog(@"number of rows in section");

    NSInteger rows = 0;
    
    /*
     The number of rows depends on the section.
     In the case of ingredients, if editing, add a row in editing mode to present an "Add Ingredient" cell.
	 */
    rows = [orderLinesArray count];
    if (tableView.editing) {
//        NSLog(@"is editing");
        rows++;
    }
    return rows;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    return [PreferencesConfig7 tableView:tableView viewForHeaderInSection:section];
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    if(tableView.editing){
        //Only show header when there is one entry row + add product row
        if([_orderLineTable.dataSource tableView:_orderLineTable numberOfRowsInSection:0] > 1){
            return 40;
        }
        return 0;
    }
    else{
        return 40;
    }
    return 0;
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = nil;
    if (indexPath.row < orderLinesArray.count) {
        static NSString *CellIdentifier = @"OrderLineCell";
        cell = (OrderLineCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil)
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"OrderLineCell" owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }
        OrderLineDataModel *currentOLDM = [orderLinesArray objectAtIndex:indexPath.row];
        [((OrderLineCell*)cell) setOrderLineQuantityValue:[NSString stringWithFormat:@"%@", currentOLDM.orderLineQuantity]];
        [((OrderLineCell*)cell) setOrderLineCodeValue:currentOLDM.orderLineCode];
        
        NSNumberFormatter *fmt = [[NSNumberFormatter alloc] init];
        [fmt setPositiveFormat:@"0.##"];
        [((OrderLineCell*)cell) setOrderLinePriceValue:[NSString stringWithFormat:@"£%@", currentOLDM.orderLinePrice]];
//        NSLog(@"setting cell price to: %@",[NSString stringWithFormat:@"£%@", [fmt stringFromNumber:currentOLDM.orderLineTotal]]);
        [((OrderLineCell*)cell) setOrderLineTotalValue:[NSString stringWithFormat:@"£%@", [fmt stringFromNumber:currentOLDM.orderLineTotal]]];
        
//        ((OrderLineCell*)cell).orderLineQuantity.text = [NSString stringWithFormat:@"%@", currentOLDM.orderLineQuantity];
//        ((OrderLineCell*)cell).orderLineCode.text = currentOLDM.orderLineCode;
//        ((OrderLineCell*)cell).orderLinePrice.text = [NSString stringWithFormat:@"%@", currentOLDM.orderLinePrice];
//        ((OrderLineCell*)cell).orderLineTotal.text = [NSString stringWithFormat:@"%@", currentOLDM.orderLineTotal];
    } else {
        // If the row is outside the range, it's the row that was added to allow insertion (see tableView:numberOfRowsInSection:) so give it an appropriate label.
        static NSString *AddIngredientCellIdentifier = @"AddProductCell";
        
        cell = [tableView dequeueReusableCellWithIdentifier:AddIngredientCellIdentifier];
        if (cell == nil) {
            // Create a cell to display "Add Ingredient".
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:AddIngredientCellIdentifier];
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        }
        cell.textLabel.text = @"Add Product";

    }
    
    SelectedBackgroundView *selectedBackgroundViewForCell = [SelectedBackgroundView new];
    [selectedBackgroundViewForCell setBackgroundColor:[UIColor lightGrayColor]];
    cell.selectedBackgroundView = selectedBackgroundViewForCell;
    return cell;
}
//-(void)assignSelectedBackViewOnCell:(UITableViewCell*)cell{
//    UIView *selectedBackgroundViewForCell = [UIView new];
//    [selectedBackgroundViewForCell setBackgroundColor:[UIColor lightGrayColor]];
//    cell.selectedBackgroundView = selectedBackgroundViewForCell;
//}


#pragma mark -
#pragma mark Editing rows




- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
	UITableViewCellEditingStyle style = UITableViewCellEditingStyleNone;
    // In the ingredients section, the last row (row number equal to the count of ingredients) is added automatically (see tableView:cellForRowAtIndexPath:) to provide an insertion cell, so configure that cell for insertion; the other cells are configured for deletion.
    // If this is t1he last item, it's the insertion row.
    if (indexPath.row == [orderLinesArray count]) {
        style = UITableViewCellEditingStyleInsert;
    }
    else {
        style = UITableViewCellEditingStyleDelete;
    }
    
    return style;
}


// Update the data model according to edit actions delete or insert.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle
forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        [orderLinesArray removeObjectAtIndex:indexPath.row];
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationTop];
        NSLog(@"delete index: %d", (int)indexPath.row);
    }
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        [self isTableView:tableView allowedToAddNewRowForIndexPath:indexPath];
    }
    //If you delete a cell that was selected then there will be no row that is selected in the table
    //so clear the display
    if([tableView indexPathForSelectedRow] == nil){
        NSLog(@"no selection so clear display");
        [displayPad clearDisplays];
    };
    
    
    NSLog(@"number of rows in tableView: %d", [tableView numberOfRowsInSection:0]);
    [self hideDisplayBasedOnNumberOfRowsInTableView:tableView];
}
-(void)hideDisplayBasedOnNumberOfRowsInTableView:(UITableView*)tableView{
    NSInteger rows = [tableView numberOfRowsInSection:0];
    if(rows > 1){ //1 is the actual `add product` cell
        [self setDisplayHidden:NO];
    }
    else{
        [self setDisplayHidden:YES];
    }
}

-(void)setDisplayHidden:(BOOL)shouldHide{
    if(shouldHide){
        if([numberPad.view isHidden] == FALSE){
            [numberPad.view setHidden:YES];
            [displayPad.view setHidden:YES];
            [orderTotalFooterView.view setHidden:YES];
            [_orderLineTable setFrame:originalTableFrame];
            NSLog(@"view frame = %@", NSStringFromCGRect(originalTableFrame));
        }
    }
    else{
        if([numberPad.view isHidden] == TRUE){
            [numberPad.view setHidden:NO];
            [displayPad.view setHidden:NO];
            [orderTotalFooterView.view setHidden:NO];
            CGRect tempFrame = _orderLineTable.frame;
            tempFrame.size.width = 596;
            _orderLineTable.frame = tempFrame;
        }
    }
    
}

-(NSDictionary*)isDataInOrderLineTableValid{
    BOOL isDataValid = YES;
    NSUInteger errorAtRow = -1;
    NSUInteger count = 0;
    NSString *error = @"";
    for(OrderLineDataModel *currentOL in orderLinesArray){
        if( ! ([currentOL.orderLineQuantity integerValue] > 0)){
            isDataValid = NO;
            errorAtRow = count;
            error = @"quantity";
            break;
        }
        if( ! (currentOL.orderLineCode.length > 3)){
            isDataValid = NO;
            errorAtRow = count;
            error = @"product code";
            break;
        }
        //        if( ! ([currentOL.orderLinePrice floatValue] > 0)){
        //            canAddNewRow = NO;
        //            errorAtRow = count;
        //            error = @"price";
        //            break;
        //        }
        //        if( ! ([currentOL.orderLineTotal floatValue] > 0)){
        //            canAddNewRow = NO;
        //            errorAtRow = count;
        //            error = @"blah blu";
        //            break;
        //        }
        count++;
    }
    //We start the index from 1 instead of 0;
    errorAtRow++;
    
    return @{@"isDataValid":[NSNumber numberWithBool:isDataValid], @"errorMessage":error, @"errorIndex":[NSNumber numberWithInteger:errorAtRow]};
    
}
-(void)isTableView:(UITableView*)tableView allowedToAddNewRowForIndexPath:(NSIndexPath*)indexPath{
    NSDictionary *dataValidityStatus = [self isDataInOrderLineTableValid];
    /*
     @"isDataValid"
     @"error"
     @"errorIndex"
     */
    int errorAtRow = [[dataValidityStatus objectForKey:@"errorIndex"] integerValue];
    
    if([[dataValidityStatus objectForKey:@"isDataValid"] boolValue]){
        NSLog(@"index: %d", (int)indexPath.row);
        OrderLineDataModel *oLDM = [[OrderLineDataModel alloc] init];
        [orderLinesArray insertObject:oLDM atIndex:[orderLinesArray count]];
        
        [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationBottom];
        [_orderLineTable selectRowAtIndexPath:indexPath animated:YES scrollPosition:UITableViewScrollPositionNone];
        [tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:indexPath.row+1 inSection:0] atScrollPosition:UITableViewScrollPositionBottom animated:YES];
        [displayPad clearDisplays];
        [displayPad moveSelectionBorderToBeginning];

    }
    else{
        NSIndexPath *rowWithError = [NSIndexPath indexPathForRow:errorAtRow-1 inSection:0];
        [SVProgressHUD showErrorWithStatus:[NSString stringWithFormat:@"Complete the %@ for item: %d", [dataValidityStatus objectForKey:@"errorMessage"], errorAtRow] maskType:SVProgressHUDMaskTypeBlack];
        [tableView scrollToRowAtIndexPath:rowWithError atScrollPosition:UITableViewScrollPositionTop animated:YES];
        [tableView selectRowAtIndexPath:rowWithError animated:YES scrollPosition:UITableViewScrollPositionNone];
        
//#warning Display alertview message for when a row cannot be added due to fields being empty
    }
}
#pragma mark -
#pragma mark Moving rows

- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    BOOL canMove = NO;
    // Moves are only allowed within the ingredients section.  Within the ingredients section, the last row (Add Ingredient) cannot be moved.
    canMove = indexPath.row != [orderLinesArray count];
    return canMove;
}


- (NSIndexPath *)tableView:(UITableView *)tableView targetIndexPathForMoveFromRowAtIndexPath:(NSIndexPath *)sourceIndexPath toProposedIndexPath:(NSIndexPath *)proposedDestinationIndexPath {
    NSIndexPath *target = proposedDestinationIndexPath;
    
    /*
     Moves are only allowed within the ingredients section, so make sure the destination is in the ingredients section.
     If the destination is in the ingredients section, make sure that it's not the Add Ingredient row -- if it is, retarget for the penultimate row.
     */
	
//    if (proposedSection < INGREDIENTS_SECTION) {
//        target = [NSIndexPath indexPathForRow:0 inSection:INGREDIENTS_SECTION];
//    } else if (proposedSection > INGREDIENTS_SECTION) {
//        target = [NSIndexPath indexPathForRow:([recipe.ingredients count] - 1) inSection:INGREDIENTS_SECTION];
//    } else {
    NSUInteger ingredientsCount_1 = [orderLinesArray count] - 1;
    
    if (proposedDestinationIndexPath.row > ingredientsCount_1) {
        target = [NSIndexPath indexPathForRow:ingredientsCount_1 inSection:0];
    }
//    }
    return target;
}


- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
	
	/*
	 Update the ingredients array in response to the move.
	 Update the display order indexes within the range of the move.
	 */
    OrderLineDataModel *fromOLDM = [orderLinesArray objectAtIndex:fromIndexPath.row];
    [orderLinesArray removeObjectAtIndex:fromIndexPath.row];
    [orderLinesArray insertObject:fromOLDM atIndex:toIndexPath.row];
//	NSInteger start = fromIndexPath.row;
//	if (toIndexPath.row < start) {
//		start = toIndexPath.row;
//	}
//	NSInteger end = toIndexPath.row;
//	if (fromIndexPath.row > end) {
//		end = fromIndexPath.row;
//	}
//	for (NSInteger i = start; i <= end; i++) {
//		ingredient = [ingredients objectAtIndex:i];
//		ingredient.displayOrder = [NSNumber numberWithInteger:i];
//	}
}



#pragma mark - PkNumberPad delegate methods
-(void)numberPadView:(PkNumberPadVC *)pkNumberPadVC digitPressed:(NSNumber*)digitPressed{
    
//    NSString *string = [[displayPad getSelectedLabel] text];
//    [string stringByAppendingString:[digitPressed stringValue]];
    [displayPad setDisplayPadSelectedLabelValue:[digitPressed stringValue]];

}

-(void)numberPadViewBackspaceButtonDidPress:(PkNumberPadVC *)numberPadView{
    UILabel *currentLabel = [displayPad getSelectedLabel];
    NSMutableString *string = [currentLabel.text mutableCopy];
    if([string length] > 0){
        string = [[string substringWithRange:NSMakeRange(0, [string length] -1)] mutableCopy];
    }
    if ([string length] == 0) {
        //If the label is quantity then reset back to 1 since you have to buy a minimum of 1
        if(currentLabel.tag == 1){
            string = [NSMutableString stringWithFormat:@"0"];
        }
        else{
            string = [NSMutableString stringWithFormat:@"0"];
        }
    }
    [currentLabel setText:string];
    NSLog(@"backspace");
}

-(void)numberPadViewPointButtonDidPress:(PkNumberPadVC *)numberPadView{
    NSLog(@"point");
   
    //Find out which labe is currently selected
    OrderTakingLabelType currentLabelType = [displayPad getSelectedLabelName];
    switch (currentLabelType) {
        case OrderTakingLabelType_Price:
            [self addPointSymbolToOrderLineCell];
            break;
        default:
            break;
    }
}

-(void)addPointSymbolToOrderLineCell{
    UILabel *currentLabel = [displayPad getSelectedLabel];
    NSMutableString *string = [currentLabel.text mutableCopy];
    if([string rangeOfString:@"."].location == NSNotFound){
        if([string hasPrefix:@"0"] == FALSE){ //&& [string length] > 0){
            [string appendString:@"."];
            [currentLabel setText:string];
        }
        else{
            [string appendString:@"."];
            [currentLabel setText:string];
        }
    }
}
-(void)numberPadViewReturnButtonDidPress:(PkNumberPadVC *)numberPadView{
    NSLog(@"enter");
    [self enterKeyPressed];

    //If no cell is selected, and enter is pressed, alert th user to select a cell
    if([_orderLineTable indexPathForSelectedRow] == nil){
        [SVProgressHUD showErrorWithStatus:@"Select an item to edit" maskType:SVProgressHUDMaskTypeBlack];
    }
    else{
        if([orderLinesArray count] > 0){
            NSIndexPath *selectedRow = [_orderLineTable indexPathForSelectedRow];
            NSLog(@"selected row: %d", selectedRow.row);
            OrderLineDataModel *oLDM = [orderLinesArray objectAtIndex:selectedRow.row];
            if([displayPad getSelectedLabel] == displayPad.quantityLabel){
                if([displayPad.quantityLabel.text isEqualToString:@"0"]){
                    [[displayPad quantityLabel] setText:@"1"];
                }
            }
            
          
            
            
            NSString *quantity = [displayPad quantityLabel].text;
            NSString *code = [displayPad productLabel].text;
            NSString *price = [displayPad priceLabel].text;
            if([price hasSuffix:@"."]){
                price = [price substringToIndex:[price length]-1];
            }
            price = [NSString stringWithFormat:@"%g", [price floatValue]];
            
            [oLDM setOrderLineQuantity:[NSNumber numberWithInteger:[quantity integerValue]]];
            [oLDM setOrderLineCode:code];
            [oLDM setOrderLinePrice:[NSNumber numberWithFloat:[price floatValue]]];

            
            [oLDM setOrderLineTotal:[NSNumber numberWithFloat:((float)[quantity integerValue] * [price floatValue])]];
            
            [[displayPad priceLabel] setText:price];
            [_orderLineTable reloadData];
            [_orderLineTable selectRowAtIndexPath:selectedRow animated:NO scrollPosition:UITableViewScrollPositionTop];

    //        for(OrderLineDataModel *currentOLDM in orderLinesArray){
    //            currentOLDM
    //        }
            [self calculateTotalWithItems];
        }
        else{
            [_orderLineTable reloadData];

        }
    }
    
    [self shouldSelectionBoxCycleThroughDisplayPadOrAddNewOrderItemRow];
    
    
 /*
    UILabel *currentLabel = [displayPad getSelectedLabel];
    NSString *string = [currentLabel text];
    
    
//    OrderLineCell *cell = (OrderLineCell*)[_orderLineTable cellForRowAtIndexPath:selectedRow];

    OrderTakingLabelType currentLabelType = [displayPad getSelectedLabelName];
    
    switch (currentLabelType) {
        case OrderTakingLabelType_Quantity:
            [oLDM setOrderLineQuantity:[NSNumber numberWithInteger:[string integerValue]]];
            break;
            
        case OrderTakingLabelType_ProductCode:
            [oLDM setOrderLineCode:string];
//            [cell setOrderLineCodeValue:string];
            break;
            
        case OrderTakingLabelType_Price:
            [oLDM setOrderLinePrice:[NSNumber numberWithFloat:[string floatValue]]];
//            [cell setOrderLinePriceValue:string];
            //When pressing enter on an imcomplete decimal entry such as "200.", the point needs to disappear
            //Float seems to not save the decimal point unless its followed by another number
            //So cast float to string and then update the label which will get rid of any premature decimal points
//            [currentLabel setText:[NSString stringWithFormat:@"%f",([string floatValue] == 0) ? 0]];
            NSLog(@"string: %@ , currentLabel: %@",string, currentLabel.text);
            if([string hasSuffix:@"."]){
                string = [string substringToIndex:[string length]-1];
            }
            [currentLabel setText:string];
            break;
        default:
            break;
    }
  */
    
}

-(void)enterKeyPressed{
    //WHERE CORE DATA STUFF HAPPENS
}


-(void)shouldShowReceipt:(BOOL)showReceipt{
    NSLog(@"show receipt: %@", showReceipt == YES ? @"YES" : @"NO");
    CGRect frame = _orderLineTable.frame;

    if(showReceipt) {
        [displayPad.view setHidden:YES];
        [numberPad.view setHidden:YES];
        [_orderLineTable setEditing:NO animated:YES];
        frame.origin.x = (APP_FRAME_OF_WHOLE_SCREEN_LANDSCAPE.size.width-_orderLineTable.frame.size.width)/2+50;
        frame.size.width -= 100;
        
        CGRect rect = orderTotalFooterView.view.frame;
        rect.origin.x = frame.origin.x;
        rect.size.width -= 100;

        self.navigationItem.rightBarButtonItem = nil;

        [UIView animateWithDuration:0.24 animations:^{
            _orderLineTable.frame = frame;
            orderTotalFooterView.view.frame = rect;
            [self.view setBackgroundColor:[UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:1.0]];
        }];
    }
    else{
        [displayPad.view setHidden:NO];
        [numberPad.view setHidden:NO];
        [_orderLineTable setEditing:YES animated:YES];
        frame.origin.x = 0;
        
        frame.size.width = APP_FRAME_OF_WHOLE_SCREEN_LANDSCAPE.size.width - displayPad.view.frame.size.width;
        
        CGRect rect = orderTotalFooterView.view.frame;
        rect.origin.x = 0;
        rect.size.width = frame.size.width;
        [UIView animateWithDuration:0.24 animations:^{
            _orderLineTable.frame = frame;
            orderTotalFooterView.view.frame = rect;
            [self.view setBackgroundColor:[UIColor colorWithRed:1 green:1 blue:1 alpha:1.0]];
            [self.navigationItem setRightBarButtonItem:showReceiptButton animated:YES];
        }];
    }
    

    [_orderLineTable reloadData];
}
-(void)shouldSelectionBoxCycleThroughDisplayPadOrAddNewOrderItemRow{
//    NSLog(@"quantity value: %d product length: %d", [[displayPad.quantityLabel text] integerValue], [[displayPad.productLabel text] length]);
    
//    NSLog(@"selected row: %d, numberOfRowsInSection: %d", [_orderLineTable indexPathForSelectedRow].row, ([_orderLineTable numberOfRowsInSection:0]-2));
    if(([_orderLineTable indexPathForSelectedRow].row == ([_orderLineTable numberOfRowsInSection:0]-2)) &&
       ([[displayPad.productLabel text] length] == 4) &&
       [[displayPad.quantityLabel text] integerValue] > 0 &&
       ([displayPad getSelectedLabel] == displayPad.priceLabel)){
        
        NSLog(@"can create a new row with enter button");
//        [[_orderLineTable dataSource] tableView:_orderLineTable commitEditingStyle:UITableViewCellEditingStyleInsert forRowAtIndexPath:indexPath];
//        -(void)isTableView:(UITableView*)tableView allowedToAddNewRowForIndexPath:(NSIndexPath*)indexPath{
//        
//        if([displayPad shouldSaveCurrentTypedProductToDatabase]){
//            
//        }
        [displayPad canProcessProduct];
        [self addNewRowToOrderLineTable];
    }
    else{
        
        if([displayPad getSelectedLabel] == displayPad.productLabel){
//            NSLog(@"analysing product label");
            if([displayPad.productLabel.text length] < 4){
                [displayPad.productLabel setBackgroundColor:COLOR_WITH_RGB(249, 198, 198, 255)];
                
                return;
            }
            else{
                [displayPad.productLabel setBackgroundColor:COLOR_WITH_RGB(255, 255, 255, 255)];
            }
        }
        [displayPad moveSelectionToNextFrame];

    }

}
-(void)addNewRowToOrderLineTable{
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:[orderLinesArray count] inSection:0];
    [[_orderLineTable dataSource] tableView:_orderLineTable commitEditingStyle:UITableViewCellEditingStyleInsert forRowAtIndexPath:indexPath];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




#pragma mark - UINavigationController Delegate methods
- (void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated{
//    NSLog(@"navigationController willShowViewController class name%@", NSStringFromClass([viewController class]));
}

-(void)willMoveToParentViewController:(UIViewController *)parent{
    //If popping view controller, since parent will return null if popping.
    if(parent == NULL){
        return;
    }
//    NSLog(@"navigationController willShowViewController class name %@", NSStringFromClass([parent class]));

}

#pragma mark - UIAlertView delegate
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if(alertView == goBackToCustomerSelectionScreenAlert){
        if(buttonIndex == 1){
            [self.navigationController popToRootViewControllerAnimated:YES];        
        }
    }
    else if(alertView == goBackToOrderList){
        if(buttonIndex == 1){
            [self.navigationController popToRootViewControllerAnimated:YES];
        }
    }
}

-(void)calculateTotalWithItems{
    NSUInteger totalItems = 0;
    CGFloat totalCost = 0.0f;
    
    for(OrderLineDataModel *currentOL in orderLinesArray){
        if([currentOL.orderLineTotal floatValue] > 0){
            totalItems += [currentOL.orderLineQuantity integerValue];
            totalCost += [currentOL.orderLineTotal floatValue];
        }
    }
    [orderTotalFooterView setTotalItems:totalItems];
    [orderTotalFooterView setTotalCost:totalCost];
}
@end
