//
//  EditOrdersVC.m
//  ClassyBeads2
//
//  Created by Pavan Kataria on 14/04/2014.
//  Copyright (c) 2014 Pavan Kataria. All rights reserved.
//

#import "EditOrdersVC.h"

@interface EditOrdersVC (){
    NSMutableArray *filteredOrderLineDataArray;
    BOOL noLoadingErrors;

}

@end

@implementation EditOrdersVC
@synthesize orderTable = _orderTable;
@synthesize detailOrderTable = _detailOrderTable;
@synthesize serverRequest;

NSString *const RequestGetOrdersStateOne = @"Get/Orders";

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = @"View Orders";

    // Do any additional setup after loading the view from its nib.
    [self setNavigationItems];
    filteredOrderLineDataArray = [[NSMutableArray alloc] init];
    
}

-(void)viewDidAppear:(BOOL)animated{
    [self retrieveOrders];
}
-(void)setNavigationItems{
    UIBarButtonItem *showButton = [[UIBarButtonItem alloc] initWithTitle:@"Menu" style:UIBarButtonItemStylePlain target:self action:@selector(show:)];
    [self.navigationItem setLeftBarButtonItem:showButton animated:YES];
    self.title = @"Orders";

    
//    UIBarButtonItem *editButton = [[UIBarButtonItem alloc] initWithTitle:@"Edit Order" style:UIBarButtonItemStylePlain target:self action:@selector(editOrder)];
//    [self.navigationItem setRightBarButtonItem:editButton animated:YES];
    
    UIBarButtonItem *editButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemEdit target:self action:@selector(editOrder)];//:@"Menu" style:UIBarButtonItemStylePlain target:self action:@selector(show:)];

    UIBarButtonItem *emailButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCompose target:self action:@selector(sendEmail)];//:@"Menu" style:UIBarButtonItemStylePlain target:self action:@selector(show:)];

    UIBarButtonItem *emptyButton = [[UIBarButtonItem alloc] initWithTitle:@"  " style:UIBarButtonItemStylePlain target:self action:nil];
    [self.navigationItem setRightBarButtonItems:@[editButton, emptyButton, emptyButton, emailButton] animated:YES];

    
    [self.navigationItem setLeftBarButtonItem:showButton animated:YES];
    

}
-(void)sendEmail{
    if(![_orderTable indexPathForSelectedRow]){
        [SVProgressHUD showErrorWithStatus:@"Select an order to send a receipt" maskType:SVProgressHUDMaskTypeBlack];// afterDelay:1.5];
        return;
    }
    
    NSIndexPath *selectedIndex = [_orderTable indexPathForSelectedRow];
    OrdersDataModel *odm = [orderDataArray objectAtIndex:selectedIndex.row];
    if([odm.orderCustomerEmail isEqual:[NSNull null]]){
        
//        [SVProgressHUD dismissWithError:[NSString stringWithFormat:@"User: %@, does not have an email address, set email address for person in the customers panel", odm.orderCustomerName]];// afterDelay:1.5];
        [SVProgressHUD showErrorWithStatus:[NSString stringWithFormat:@"User: %@, does not have an email address, go to customers panel to set their email address", odm.orderCustomerName] maskType:SVProgressHUDMaskTypeBlack];

        NSLog(@"NO EMAIL FOR %@", odm.orderCustomerName);
        return;
    }
    
    
    NSString *emailTitle = [NSString stringWithFormat:@"Classy Beads Receipt: %@", odm.orderReferenceNumber];
//    NSString *messageBody = @"Hey, check this out!";
    NSArray *toRecipents = [NSArray arrayWithObject:odm.orderCustomerEmail];
    
    MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
    mc.mailComposeDelegate = self;
    [mc setSubject:emailTitle];
//    [mc setMessageBody:messageBody isHTML:NO];
    [mc setToRecipients:toRecipents];
    
    
    
    
    NSMutableString *emailBody = [NSMutableString stringWithFormat:@"<p><b>Hello %@,<br>here are your receipt details:<br> Order Reference #:</b> %@ <br><b>Purchase Date:</b> %@</p><br>", odm.orderCustomerName, odm.orderReferenceNumber, odm.orderCreatedDate];
    
    NSString *beginningOfTableHTMLString = @"<table style=\"width:300px\">";
    [emailBody appendString:beginningOfTableHTMLString];
    
    NSMutableArray *orderLines = [self getOrderLinesForOrder:odm];
    for(int i = 0; i < [orderLines count]; i++){
        [emailBody appendString:[[orderLines objectAtIndex:i] orderLineConvertToTableDataRow]];
    }
    NSString *endOfTableHTMLString = @"</table>";
    [emailBody appendString:endOfTableHTMLString];
    
    [mc setMessageBody:emailBody isHTML:YES];
    
//    // Determine the file name and extension
//    NSArray *filepart = [file componentsSeparatedByString:@"."];
//    NSString *filename = [filepart objectAtIndex:0];
//    NSString *extension = [filepart objectAtIndex:1];
//    
//    // Get the resource path and read the file using NSData
//    NSString *filePath = [[NSBundle mainBundle] pathForResource:filename ofType:extension];
//    NSData *fileData = [NSData dataWithContentsOfFile:filePath];
//    
//    // Determine the MIME type
//    NSString *mimeType;
//    if ([extension isEqualToString:@"jpg"]) {
//        mimeType = @"image/jpeg";
//    } else if ([extension isEqualToString:@"png"]) {
//        mimeType = @"image/png";
//    } else if ([extension isEqualToString:@"doc"]) {
//        mimeType = @"application/msword";
//    } else if ([extension isEqualToString:@"ppt"]) {
//        mimeType = @"application/vnd.ms-powerpoint";
//    } else if ([extension isEqualToString:@"html"]) {
//        mimeType = @"text/html";
//    } else if ([extension isEqualToString:@"pdf"]) {
//        mimeType = @"application/pdf";
//    }
//    
//    // Add attachment
//    [mc addAttachmentData:fileData mimeType:mimeType fileName:filename];
//    
    // Present mail view controller on screen
    [self presentViewController:mc animated:YES completion:NULL];
    
}

- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            break;
    }
    
    // Close the Mail Interface
    [self dismissViewControllerAnimated:YES completion:NULL];
}


-(void)editOrder{
    if([_orderTable indexPathForSelectedRow]){
        
        NSIndexPath *selectedIndex = [_orderTable indexPathForSelectedRow];
        OrdersDataModel *odm = [[OrdersDataModel alloc] init];
        odm = [orderDataArray objectAtIndex:selectedIndex.row];
        
        [odm setOrderLinesDataArray:[self getOrderLinesForOrder:odm]];

        TakeOrderVC *takeOrderVC = [[TakeOrderVC alloc] initInOrderEditModeWithOrderObject:odm];
        
        [((PKNavigationController*)[self.sideMenuViewController mainViewController]) pushViewController:takeOrderVC animated:YES];
    }
    else{
        [SVProgressHUD showErrorWithStatus:@"Select an order to edit" maskType:SVProgressHUDMaskTypeBlack];
    }
    
}
-(void)show:(id)sender{
    //    [[[AppDelegate getAppDelegateReference] splitViewController] toggleMasterView:sender];
    [self.sideMenuViewController openMenuAnimated:YES completion:nil];
}


#pragma mark - ASIHTTPRequest Delegate methods
-(void)retrieveOrders{
    [SVProgressHUD showWithStatus:@"Getting orders" maskType:SVProgressHUDMaskTypeBlack];
//    [editOrdersSegmentControl setSelectedSegmentIndex:0];
//    [NoResultsView noResultsView:noResultsContainer
//              setContainerHidden:NO];
//    [NoResultsView noResultsView:noResultsContainer setCenterWithAutoCorrection:self.view.center];
//    [NoResultsView noResultsView:noResultsContainer hideTables:YES tables:_orderTable, _detailOrderTable, nil];
//    [NoResultsView noResultsView:noResultsContainer setContainerText:@"Loading orders"];
//    
//    
    [self retrieveWithRequestStringType:RequestGetOrdersStateOne withDictionary:nil];
    [filteredOrderLineDataArray removeAllObjects];
    [orderDataArray removeAllObjects];
//    [filteredOrderDataArray removeAllObjects];
    [_orderTable reloadData];
    [_detailOrderTable reloadData];
    
}


-(void)retrieveWithRequestStringType:(NSString*)typeOfRequest withDictionary:(NSMutableDictionary*)inputdictionary{

    
    NSLog(@"Retrieve %@ method called", typeOfRequest);
    NSString *urlString = [NSString stringWithFormat:@"%@/Secure/CB/%@", @"http://www.riabp.com/CB", RequestGetOrdersStateOne];
    NSString *encodedUrlString = [urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSURL *url = [[NSURL alloc] initWithString:encodedUrlString];
    serverRequest = nil;
    serverRequest = [ASIFormDataRequest requestWithURL:url];
    [serverRequest addRequestHeader:@"Content-Type" value:@"application/json"];
    [serverRequest addRequestHeader:@"Request-Method" value:@"POST"];
    
    if([typeOfRequest isEqualToString:RequestGetOrdersStateOne]){
        [serverRequest setUserInfo:[NSDictionary dictionaryWithObject:RequestGetOrdersStateOne forKey:@"RequestType"]];
    }
    [serverRequest setDelegate:self];
    [serverRequest setDidFinishSelector:@selector(requestSucceeded:)];
    [serverRequest setDidFailSelector:@selector(requestFailed:)];
    [serverRequest startAsynchronous];
    
}

-(void)requestSucceeded:(ASIHTTPRequest*)request{
    
    NSInteger statusCode = [[[request responseHeaders] objectForKey:@"StatusCode"] intValue];
    NSLog(@"StatusCode: %@", [[request responseHeaders] objectForKey:@"StatusCode"]);
    
    NSString *myString = [[NSString alloc] initWithData:[request responseData] encoding:NSUTF8StringEncoding];
    NSLog(@"MY STRING:%@ ", myString);
    NSDictionary *JSONDictionary = [myString JSONValue];
    
    switch (statusCode) {
        case 400:
        case 401:
        {
            NSLog(@"display error message");
            [SVProgressHUD showErrorWithStatus:[NSString stringWithFormat:@"Error because: %@",[[request.responseString JSONValue] objectForKey:@"Message"]]];
            
            break;
        }
        case 200:
            NSLog(@"status code = 200 so successful");
            
            //orderRetrieval succeeded
            if([[[request userInfo] objectForKey:@"RequestType"] isEqualToString:RequestGetOrdersStateOne]){
                //                [[AppDelegate getAppDelegateReference] dismissLoadingAlert];
                [SVProgressHUD dismiss];
                noLoadingErrors = TRUE;
                [self setupOrderTableWithDictionary:[JSONDictionary objectForKey:@"orderResults"]];
                //[self setupFactoriesSVWithDictionary:[JSONDictionary objectForKey:@"factoriesResult"]];
                [self setupDetailedOrderArrayWithDictionary:[JSONDictionary objectForKey:@"detailedOrderResults"]];
//                [orderTableRefreshControl endRefreshing];
            }
            
            break;
        default:
            NSLog(@"went to none: %d or %@", statusCode, [[request responseHeaders] objectForKey:@"StatusCode"]);
            break;
    }
}
-(void)requestFailed:(ASIHTTPRequest *)request{
    [SVProgressHUD dismiss];

    if([[[request userInfo] objectForKey:@"RequestType"] isEqualToString:RequestGetOrdersStateOne]){
        //############# check internet connection over here
        NSLog(@"FLOPPED: %@, so trying again", RequestGetOrdersStateOne);
        [self retrieveOrders];
    }
}


-(void)setupDetailedOrderArrayWithDictionary:(NSDictionary*)inputJSONDictionary{
    //NSLog(@"detailed order array: %@", inputJSONDictionary);
    if(detailOrderDataArray == nil){
        detailOrderDataArray = [[NSMutableArray alloc] init];
    }
    [detailOrderDataArray removeAllObjects];
    [filteredOrderLineDataArray removeAllObjects];
    
    for(id key in inputJSONDictionary){
        OrderLineDataModel *odm = [[OrderLineDataModel alloc] initWithJSONData:key];
        [detailOrderDataArray addObject:odm];
    }
}
-(void)setupOrderTableWithDictionary:(NSDictionary*)inputJSONDictionary{
    NSLog(@"ORDER ARRAY: %@", inputJSONDictionary);
    if(orderDataArray == nil){
        orderDataArray = [[NSMutableArray alloc] init];
    }
    [orderDataArray removeAllObjects];
    
    for(id key in inputJSONDictionary){
        OrdersDataModel *odm = [[OrdersDataModel alloc] initWithJSONData:key];
        [orderDataArray addObject:odm];
    }
    [_orderTable reloadData];
    //so that any data on the right is also reloaded with new data thats come in from above.
    [_detailOrderTable reloadData];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




#pragma mark - UITableView Data Source

-(void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath{
    //    NSLog(@"cell at indexPath: %d became deselected", indexPath.row);
    if(tableView == _orderTable){
        AgentDisplayCell *cell = (AgentDisplayCell*)[tableView cellForRowAtIndexPath:indexPath];
        
        CATransition *animation = [CATransition animation];
        animation.duration = 0.6;
        animation.type = kCATransitionFade;
        animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
        [cell.agentBalanceLabel.layer addAnimation:animation forKey:@"changeTextTransition"];
        
        // Change the text
        cell.agentBalanceLabel.text = @"";
        OrdersDataModel *odm = [orderDataArray objectAtIndex:indexPath.row];
        [odm setStateSelected:NO];
    }
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if(tableView == _orderTable) return [orderDataArray count];
    else if(tableView == _detailOrderTable) return [filteredOrderLineDataArray count];
    else return 0;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(tableView == _orderTable){
        if([orderDataArray count] == 0){
            static NSString *cellID = @"loadingcell";
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
            if (!cell) {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                
            }
            if(indexPath.row == 3){
                if(noLoadingErrors){
                    cell.textLabel.text = [NSString stringWithFormat:@"No orders are pending"];
                }
                else{
                    cell.textLabel.text = [NSString stringWithFormat:@"Loading Orders"];
                }
                [cell.textLabel setBackgroundColor:[UIColor clearColor]];
                
                //[cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
            }
            return cell;
        }
        else{
            static NSString *fellID = @"productcell";

            AgentDisplayCell *orderDisplayCell = (AgentDisplayCell *)[tableView dequeueReusableCellWithIdentifier:fellID];
            if ( ! orderDisplayCell)
            {
                NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"AgentDisplayCell" owner:self options:nil];
                orderDisplayCell = [nib objectAtIndex:0];

                [orderDisplayCell.agentNameLabel setTextColor:[UIColor blackColor]];
                [orderDisplayCell.agentNameLabel setFont:[UIFont fontWithName:@"CartoGothicStd-Bold" size:23.0]];

                CGRect bF = [orderDisplayCell.agentBalanceLabel frame];
                [orderDisplayCell.agentBalanceLabel setFrame:CGRectMake(bF.origin.x, 30, bF.size.width, orderDisplayCell.agentStateLabel.frame.size.height)];
                [orderDisplayCell.agentBalanceLabel setFont:[UIFont systemFontOfSize:20]];
                
                orderDisplayCell.agentBalanceLabel.adjustsFontSizeToFitWidth = YES;
                orderDisplayCell.agentNameLabel.adjustsFontSizeToFitWidth = YES;
                
                
                [orderDisplayCell.agentNameLabel setHighlightedTextColor:APP_COLOR_WHITE];
                [orderDisplayCell.agentBalanceLabel setHighlightedTextColor:APP_COLOR_WHITE];
                
                orderDisplayCell.selectedBackgroundView = [PreferencesConfig7 getDefaultSelectedBackgroundView];
                [orderDisplayCell setBackgroundColor:[UIColor whiteColor]];//
                
            }
            OrdersDataModel *odm = [orderDataArray objectAtIndex:indexPath.row];

            orderDisplayCell.agentNameLabel.text = odm.orderReferenceNumber;
            orderDisplayCell.agentStateLabel.text = @"";
            [orderDisplayCell.agentStateLabel setTextColor:[UIColor darkGrayColor]];
            
            orderDisplayCell.agentCardinalDirectionLabel.text = @"";

            [orderDisplayCell setSelectionStyle:UITableViewCellSelectionStyleBlue];
            
            if([odm stateSelected]){
                orderDisplayCell.agentBalanceLabel.text = [NSString stringWithFormat:@"£%@", odm.orderTotalAmount];
            }
            else{
                orderDisplayCell.agentBalanceLabel.text = @"";
            }
            

//            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:fellID];
            
//            if (!cell) {
//                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:fellID];
//                
//                [cell.textLabel setFont:[UIFont fontWithName:@"CartoGothicStd-Bold" size:23.0]];
//                [cell.textLabel setTextColor:[UIColor blackColor]];
//                cell.textLabel.adjustsFontSizeToFitWidth = YES;
//                [cell.textLabel setHighlightedTextColor:APP_COLOR_WHITE];
//                cell.selectedBackgroundView = [PreferencesConfig7 getDefaultSelectedBackgroundView];
//                [cell setBackgroundColor:[UIColor whiteColor]];
//            }
//            [cell.textLabel setText:odm.orderReferenceNumber];
            return orderDisplayCell;
        }
    }
    else if(tableView == _detailOrderTable){
        if([filteredOrderLineDataArray count] == 0){
            static NSString *cellID = @"loadingdetailcell";
            
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
            if (!cell) {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                
            }
            if(indexPath.row == 2){
                cell.textLabel.text = [NSString stringWithFormat:@"Select an order to view"];
                [cell.textLabel setBackgroundColor:[UIColor clearColor]];
                //[cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
            }
            else{
                cell.textLabel.text = [NSString stringWithFormat:@""];
                [cell.textLabel setBackgroundColor:[UIColor clearColor]];
            }
            return cell;
        }
        else{
            OrderLineDataModel *oldm = [filteredOrderLineDataArray objectAtIndex:indexPath.row];
            
            static NSString *cellID = @"CellIdentifier33";
            
            OrderLineCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
            if (cell == nil)
            {
                NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"OrderLineCell" owner:self options:nil];
                cell = [nib objectAtIndex:0];
                //                [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
            }
            // ... set up the cell here ...
            
            [cell setOrderLineQuantityValue:[NSString stringWithFormat:@"%d", [oldm.orderLineQuantity integerValue]]];
            [cell setOrderLineCodeValue:[NSString stringWithFormat:@"%@", oldm.orderLineCode]];
            [cell setOrderLinePriceValue:[NSString stringWithFormat:@"%.2f", [oldm.orderLinePrice floatValue]]];
            [cell setOrderLineTotalValue:[NSString stringWithFormat:@"= %.2f", [oldm.orderLineTotal floatValue]]];

            return cell;
        }
    }
    else{
        return nil;
    }
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(tableView == _orderTable){
        return 70;
    }
    else{
        return 55;
    }
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return [PreferencesConfig7 tableView:tableView heightForHeaderInSection:section];
}

-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    return [PreferencesConfig7 tableView:tableView viewForHeaderInSection:section withFontSize:APP_DEFAULT_HEADER_FONT_SIZE];
}
-(NSString*)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    if(_orderTable) return @"Orders";
    else if(_detailOrderTable) return @"Order Items";
    else return @"ERROR CONTACT PAVAN";
}

#pragma mark - UITableView Delegate Methods
-(NSIndexPath*)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if([_orderTable indexPathForSelectedRow]){
        NSIndexPath *selectedIndexPath = [_orderTable indexPathForSelectedRow];
        if(selectedIndexPath.row == indexPath.row){
            return nil;
        }
        return indexPath;
    }
    else{
        return indexPath;
    }
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if(tableView == _orderTable){
        if([orderDataArray count] > 0){
         
            OrdersDataModel *odm = [orderDataArray objectAtIndex:indexPath.row];
            [self animateAgentBalance:YES withTable:tableView indexPath:indexPath];

//            [self enableDeleteButton:YES];
            
            [filteredOrderLineDataArray removeAllObjects];
            
            filteredOrderLineDataArray = [self getOrderLinesForOrder:odm];
            
            //NSLog(@"filtered order line data array: %@", filteredOrderLineDataArray);
            [_detailOrderTable beginUpdates];
            [_detailOrderTable deleteSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationRight];
            [_detailOrderTable insertSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationLeft];
            [_detailOrderTable endUpdates];
            
        }
    }
}
-(void)animateAgentBalance:(BOOL)shouldAnimate withTable:(UITableView*)tableView indexPath:(NSIndexPath*)indexPath{
    if(shouldAnimate){
        OrdersDataModel *odm = [orderDataArray objectAtIndex:indexPath.row];

        [odm setStateSelected:YES];
        
        NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
        [numberFormatter setPositiveFormat:@"###,###,###,###,##0"];
        NSLog(@"selected agent: %@ with %@ balance", odm.orderCustomerName, odm.orderTotalAmount);
//        NSInteger ba = [odm.orderTotalAmount floatValue];
//        [numberFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
//        [numberFormatter setMaximumFractionDigits:2];
        [numberFormatter setPaddingPosition:NSNumberFormatterPadBeforePrefix];
        [numberFormatter setPaddingCharacter:@"0"];
        [numberFormatter setMinimumIntegerDigits:10];
        NSString *finalStringBalance = [NSString stringWithFormat:@"£   %.02f", [[numberFormatter numberFromString:odm.orderTotalAmount] floatValue]];
//        NSLog(@"NUMBER : %@",[numberFormatter numberFromString:odm.orderTotalAmount]);
        NSLog(@"final string balance: %@", finalStringBalance);
        AgentDisplayCell *cell = (AgentDisplayCell*)[tableView cellForRowAtIndexPath:indexPath];
        
        
        CATransition *animation = [CATransition animation];
        animation.duration = 1.0;
        animation.type = kCATransitionFade;
        animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
        [cell.agentBalanceLabel.layer addAnimation:animation forKey:@"changeTextTransition"];
        
        // Change the text
        cell.agentBalanceLabel.text = finalStringBalance;
        
    }
}

-(NSMutableArray*)getOrderLinesForOrder:(OrdersDataModel*)odm{
    NSMutableArray *tempArray = [[NSMutableArray alloc] init];
    for(OrderLineDataModel *o in detailOrderDataArray){
        if(odm.orderId == o.orderLineOrderId){
            [tempArray addObject:o];
        }
    }
    return tempArray;
}
@end
