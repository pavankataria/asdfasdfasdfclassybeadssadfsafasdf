//
//  ReceiptVC.m
//  ClassyBeads2
//
//  Created by Pavan Kataria on 09/03/2014.
//  Copyright (c) 2014 Pavan Kataria. All rights reserved.
//

#import "ReceiptVC.h"

@interface ReceiptVC (){
    BOOL EDIT_MODE;
}

@end

@implementation ReceiptVC
@synthesize invoiceTable = _invoiceTable;
@synthesize invoiceDataArray = _invoiceDataArray;
@synthesize serverRequest;
@synthesize customerDetails = _customerDetails;
@synthesize editModeOrderObject = _editModeOrderObject;
typedef enum{
    kTagCreateOrder = 3434,
    //    kRetrieveProducts = 5434,
    //    kAddProduct = 2324,
    //    kRetrieveAllProductsPrices = 3467,
    //    kUpdateProduct = 8753
}RequestTypeTag;


NSString *const RequestForCreateOrder = @"Create/Order";
NSString *const RequestForEditOrder = @"Update/Order";


-(id) initInEditModeWithOrderObject:(OrdersDataModel*)orderObject{
    self = [super init];
    if (!self) return nil;
    _editModeOrderObject = orderObject;
    
    _invoiceDataArray = [[NSMutableArray alloc] init];
    _invoiceDataArray = _editModeOrderObject.orderLinesDataArray;
    NSLog(@"Original Total Order amount: %@", _editModeOrderObject.orderTotalAmount);

    EDIT_MODE = YES;
    return self;

};

-(id) initWithCustomerObject:(CustomerDataModel*)customerObject andInvoiceDataArray:(NSMutableArray*)invoiceDataArray{
    self = [super init];
    if(!self) return nil;
    _customerDetails = customerObject;
    _invoiceDataArray = invoiceDataArray;
    EDIT_MODE = NO;
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self.view setBackgroundColor:[UIColor blackColor]];
    
    CGRect tempFrame;
    orderTotalFooterView = [[OrderTotalFooterVC alloc] init];
    tempFrame = orderTotalFooterView.view.frame;
    tempFrame.size.width = _invoiceTable.frame.size.width;
    tempFrame.origin.x = _invoiceTable.frame.origin.x;
    tempFrame.size.height = ORDER_ITEM_ROW_HEIGHT;
    tempFrame.origin.y = self.view.frame.size.height-tempFrame.size.height;
    
    [orderTotalFooterView.view setFrame:tempFrame];
    [self.view addSubview:orderTotalFooterView.view];
    
    
    
    
    NSUInteger totalItems = 0;
    CGFloat totalCost = 0.0f;
    
    for(OrderLineDataModel *currentOL in _invoiceDataArray){
        if([currentOL.orderLineTotal floatValue] > 0){
            totalItems += [currentOL.orderLineQuantity integerValue];
            totalCost += [currentOL.orderLineTotal floatValue];
        }
    }
    [orderTotalFooterView setTotalItems:totalItems];
    [orderTotalFooterView setTotalCost:totalCost];
    NSLog(@"totalCost : %f, totalCost: %f", [orderTotalFooterView getTotalCost], totalCost);
    [self setNavigationRightButton];
    
    
    tempFrame = _invoiceTable.frame;
    tempFrame.size.height -= orderTotalFooterView.view.frame.size.height;
    _invoiceTable.frame = tempFrame;
}



-(void)setNavigationRightButton{
    if (EDIT_MODE) {
        buttonProcessOrder = [[UIBarButtonItem alloc] initWithTitle:@"Update Order" style:UIBarButtonItemStylePlain target:self action:@selector(updateOrder)];
    }
    else{
        buttonProcessOrder = [[UIBarButtonItem alloc] initWithTitle:@"Process Order" style:UIBarButtonItemStylePlain target:self action:@selector(processOrder)];
    }
    
    self.navigationItem.rightBarButtonItem = buttonProcessOrder;
}

-(void)updateOrder{
    [SVProgressHUD showWithStatus:[NSString stringWithFormat:@"Updating Order: %@", _editModeOrderObject.orderReferenceNumber] maskType:SVProgressHUDMaskTypeBlack];
    NSLog(@"edit update order method called");
//    [SVProgressHUD showWithStatus:@"Updating Order" maskType:SVProgressHUDMaskTypeBlack];
    
    NSLog(@"Retrieve %@ method called", RequestForEditOrder);
    NSString *urlString = [NSString stringWithFormat:@"%@/Secure/CB/%@", @"http://www.riabp.com/CB", RequestForEditOrder];
    NSLog(@"url: %@", urlString);
    NSString *encodedUrlString = [urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSURL *url = [[NSURL alloc] initWithString:encodedUrlString];
    serverRequest = nil;
    serverRequest = [ASIFormDataRequest requestWithURL:url];
    [serverRequest addRequestHeader:@"Content-Type" value:@"    "];
    [serverRequest addRequestHeader:@"Request-Method" value:@"POST"];

    NSMutableArray *arrayToSend = [[NSMutableArray alloc] init];
    NSMutableDictionary *completeDataArray = [[NSMutableDictionary alloc] init];
    
    for(OrderLineDataModel *o in _invoiceDataArray){
        NSMutableDictionary *dictObject = [[NSMutableDictionary alloc] init]; //could use serialisation
        [dictObject setObject:o.orderLineCode forKey:@"orderLineCode"];
        [dictObject setObject:o.orderLineQuantity forKey:@"orderLineQuantity"];
        [dictObject setObject:o.orderLinePrice forKey:@"orderLinePrice"];
        [dictObject setObject:o.orderLineTotal forKey:@"orderLineTotal"];
        [arrayToSend addObject:dictObject];
    }
    [completeDataArray setObject:[NSNumber numberWithInteger:_editModeOrderObject.orderId] forKey:@"orderid"];

    [completeDataArray setObject:arrayToSend forKey:@"invoiceitems"];
    
    [completeDataArray setObject:[NSNumber numberWithFloat:[orderTotalFooterView getTotalCost]] forKey:@"totalAmount"];
    
    
    
    [completeDataArray setObject:[NSNumber numberWithInteger:_customerDetails.customerId] forKey:@"customerid"];
    
    
    [completeDataArray setObject:_editModeOrderObject.orderCustomerName forKey:@"customername"];
    [completeDataArray setObject:[NSNumber numberWithInteger:_customerDetails.customerOrderCount] forKey:@"customerordercount"];
    
    [serverRequest setTag:kTagCreateOrder];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    [dict setObject:completeDataArray forKey:@"orderobject"];
    
    NSString *jsonString = [dict JSONRepresentation];
    NSLog(@"edit OrderObject: %@", jsonString);
    
    
    [serverRequest appendPostData:[jsonString dataUsingEncoding:NSUTF8StringEncoding]];
    [serverRequest setUserInfo:[NSDictionary dictionaryWithObject:RequestForEditOrder forKey:@"RequestType"]];
    
    [serverRequest setDelegate:self];
    [serverRequest setDidFinishSelector:@selector(requestSucceeded:)];
    [serverRequest setDidFailSelector:@selector(requestFailed:)];
    [serverRequest startAsynchronous];

}
-(void)processOrder{
    NSLog(@"Process order method called");
    [SVProgressHUD showWithStatus:@"Processing Order" maskType:SVProgressHUDMaskTypeBlack];
    
    NSLog(@"Retrieve %@ method called", RequestForCreateOrder);
    NSString *urlString = [NSString stringWithFormat:@"%@/Secure/CB/%@", @"http://www.riabp.com/CB", RequestForCreateOrder];
    NSLog(@"url: %@", urlString);
    NSString *encodedUrlString = [urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSURL *url = [[NSURL alloc] initWithString:encodedUrlString];
    serverRequest = nil;
    serverRequest = [ASIFormDataRequest requestWithURL:url];
    [serverRequest addRequestHeader:@"Content-Type" value:@"    "];
    [serverRequest addRequestHeader:@"Request-Method" value:@"POST"];
    
    //    NSString *jsonString = [inputdictionary JSONRepresentation];
    
    NSMutableArray *arrayToSend = [[NSMutableArray alloc] init];
    NSMutableDictionary *completeDataArray = [[NSMutableDictionary alloc] init];
    
    for(OrderLineDataModel *o in _invoiceDataArray){
        NSMutableDictionary *dictObject = [[NSMutableDictionary alloc] init]; //could use serialisation
        
/*        //Check if there are any duplicates
        for(int i = 0; i < [arrayToSend count]; i++){
            if([[[arrayToSend objectAtIndex:i] objectForKey:@"OrderLineCode"] integerValue] == [o.orderLineCode integerValue]){
                
                int quantity = [o.orderLineQuantity integerValue] + [[[arrayToSend objectAtIndex:i] objectForKey:@"orderLineQuantity"] integerValue];
                CGFloat price = [o.orderLinePrice floatValue] + [[[arrayToSend objectAtIndex:i] objectForKey:@"orderLinePrice"] floatValue];
                CGFloat total = [o.orderLineTotal floatValue] + [[[arrayToSend objectAtIndex:i] objectForKey:@"orderLineTotal"] floatValue];
                
                //product quantity
                [[arrayToSend objectAtIndex:i] setObject:[NSNumber numberWithFloat:quantity] forKey:@"orderLineQuantity"];
                
                //product total sold
                [[arrayToSend objectAtIndex:i] setObject:[NSNumber numberWithFloat:total] forKey:@"orderLineTotal"];
            }
        }
        
        {"orderobject":{"totalAmount":5095.10009765625,"customerid":37,"customername":"Pavan","customerordercount":0,"invoiceitems":[
                                                                                                                                     
                                                                                                                                     {"orderLinePrice":3.5,"orderLineTotal":38.5,"orderLineCode":"3333","orderLineQuantity":11},{"orderLinePrice":333,"orderLineTotal":4995,"orderLineCode":"6566","orderLineQuantity":15},{"orderLinePrice":5.5999999046325684,"orderLineTotal":61.599998474121094,"orderLineCode":"3333","orderLineQuantity":11}]}}
*/
        //product name
        [dictObject setObject:o.orderLineCode forKey:@"orderLineCode"];
        
        //product quantity
        [dictObject setObject:o.orderLineQuantity forKey:@"orderLineQuantity"];
        
        //product price
        [dictObject setObject:o.orderLinePrice forKey:@"orderLinePrice"];
        
        //product total sold
        [dictObject setObject:o.orderLineTotal forKey:@"orderLineTotal"];
        
        [arrayToSend addObject:dictObject];
    }
    
    [completeDataArray setObject:arrayToSend forKey:@"invoiceitems"];
    
    [completeDataArray setObject:[NSNumber numberWithFloat:[orderTotalFooterView getTotalCost]] forKey:@"totalAmount"];
    
    
    
    [completeDataArray setObject:[NSNumber numberWithInteger:_customerDetails.customerId] forKey:@"customerid"];
    //    [completeDataArray setObject:agentDetails.agentPercentageDiscount forKey:@"customer"];
    
    
    [completeDataArray setObject:_customerDetails.customerName forKey:@"customername"];
    [completeDataArray setObject:[NSNumber numberWithInteger:_customerDetails.customerOrderCount] forKey:@"customerordercount"];
    
    
    
    
    
    /*
     OrderObject: 
     {"orderobject" : 
                {   "totalAmount":791.5,
                    "customerid":37,
                    "customername":"Pavan",
                    "customerordercount":0,
                    "invoiceitems":
                            [{  "orderLinePrice":33, "orderLineTotal":396, "orderLineCode":"3333", "orderLineQuantity":12},
                            
                            {   "orderLinePrice":33,"orderLineTotal":363,"orderLineCode":"3366","orderLineQuantity":11},
                            
                            {   "orderLinePrice":2.5,"orderLineTotal":32.5,"orderLineCode":"3333","orderLineQuantity":13}]}}
     
     */
    
    
    
    [serverRequest setTag:kTagCreateOrder];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    [dict setObject:completeDataArray forKey:@"orderobject"];
    
    //    NSLog(@"OrderObject: %@", dict);
    //        [dict setObject:[NSNumber numberWithInteger:[productQuantity.text intValue]] forKey:@"productquantityperbox"];
    //        [dict setObject:[NSNumber numberWithInteger:[productWeight.text intValue]] forKey:@"productweight"];
    NSString *jsonString = [dict JSONRepresentation];
    NSLog(@"OrderObject: %@", jsonString);
    
    
    [serverRequest appendPostData:[jsonString dataUsingEncoding:NSUTF8StringEncoding]];
    [serverRequest setUserInfo:[NSDictionary dictionaryWithObject:RequestForCreateOrder forKey:@"RequestType"]];
    
    [serverRequest setDelegate:self];
    [serverRequest setDidFinishSelector:@selector(requestSucceeded:)];
    [serverRequest setDidFailSelector:@selector(requestFailed:)];
    [serverRequest startAsynchronous];
   
}
-(void)requestSucceeded:(ASIHTTPRequest *)request{
    //NSLog(@"Request succeeded");
    NSString *myString = [[NSString alloc] initWithData:[request responseData] encoding:NSUTF8StringEncoding];
    NSLog(@"My string nsdata: %@", myString);
    //    NSLog(@"header information: %@", [request responseHeaders]);
    //    NSLog(@"StatusCode: %@", [[request responseHeaders] objectForKey:@"StatusCode"]);
    NSInteger statusCode = [[[request responseHeaders] objectForKey:@"StatusCode"] intValue];
    switch (statusCode) {
        case 400:
        case 401:
        {
            //            [[AppDelegate getAppDelegateReference] dismissLoadingAlert];
            [SVProgressHUD dismiss];
            
            //NSLog(@"display error message");
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:[[request.responseString JSONValue] objectForKey:@"Message"] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
            [alert show];
            break;
        }
        case 200:{
            //{"Error":"0", "Message":"Successfully created agent", "OrderReferenceNumber":' . $referenceCode . '}
            NSDictionary *JSONDictionary = [myString JSONValue];
            [SVProgressHUD dismiss];
            NSLog(@"status code = 200 so successful");
            NSLog(@"200 userInfo: %@", [request userInfo]);
            if([[[request userInfo] objectForKey:@"RequestType"] isEqualToString:RequestForCreateOrder]){
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Order successful" message:[NSString stringWithFormat:@"Order created"] delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
                alert.tag = 45;
                [alert show];
                [SVProgressHUD dismiss];
            }
            else if([[[request userInfo] objectForKey:@"RequestType"] isEqualToString:RequestForEditOrder]){
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Order edited sucessfully" message:[NSString stringWithFormat:@"Order updated"] delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
                alert.tag = 20;
                [alert show];
                [SVProgressHUD dismiss];
            }

            break;
        }
        default:{
            //            [[AppDelegate getAppDelegateReference] dismissLoadingAlert];
            [SVProgressHUD dismiss];
            NSLog(@"went to none userInfo: %@", [request userInfo]);
            
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Went to none" message:[NSString stringWithFormat:@"went to none: %ld or %@", (long)statusCode, [[request responseHeaders] objectForKey:@"StatusCode"]] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
            
            [alert show];
            break;
        }
    }
    [self.navigationItem.rightBarButtonItem setEnabled:YES];
}

-(void)requestFailed:(ASIHTTPRequest *)request{
    [self.navigationItem.rightBarButtonItem setEnabled:YES];
    if([request tag] == kTagCreateOrder){
        //        [[AppDelegate getAppDelegateReference] dismissLoadingAlert];
        [SVProgressHUD dismiss];
        
        //############# check internet connection over here
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Try again" message:@"Product order not sent, please send again" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alert show];
    }
}

- (void)didReceiveMemoryWarning{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableView Delegate methods
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return ORDER_ITEM_ROW_HEIGHT;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
//    if(section == 0){
        return [_invoiceDataArray count];
//    }
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if([_invoiceDataArray count] == 0){
        static NSString *cellID = @"loadingcell";
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
        if (!cell) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        if(indexPath.row == 2){
            cell.textLabel.text = [NSString stringWithFormat:@"Loading Products"];
            [cell.textLabel setBackgroundColor:[UIColor clearColor]];
        }
        return cell;
    }
    else if([_invoiceDataArray count] > 0){
        static NSString *CellIdentifier = @"OrderLineCell";
        ReceiptCell *cell = (ReceiptCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil)
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ReceiptCell" owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }
        
        OrderLineDataModel *oldm = [_invoiceDataArray objectAtIndex:indexPath.row];
        
        [cell setOrderLineQuantityValue:[NSString stringWithFormat:@"%d",[oldm.orderLineQuantity integerValue]]];//
        [cell setOrderLineCodeValue:[NSString stringWithFormat:@"   %@", oldm.orderLineCode]];
        [cell setOrderLinePriceValue:[NSString stringWithFormat:@"%.2f",[oldm.orderLinePrice floatValue]]];
        
        [cell setOrderLineTotalValue:[NSString stringWithFormat:@"%@", oldm.orderLineTotal]];
        
        return cell;
    }
    return nil;
}

//-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
//    if(section == 0){
//        return ORDER_ITEM_ROW_HEIGHT;
//    }
//    return 0;
//}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if(section == 0){
        return 40;
    }
    return 0;
}

//-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
//    if(section != 0){
//        return ROW_SECTION_HEIGHT;
//    }
//    return 0;
//}

-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    NSLog(@"viewForHeaderInSection");
    UILabel *qua = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, 100, APP_ORDER_ITEM_HEADER_HEIGHT)];
    UILabel *cod = [[UILabel alloc] initWithFrame:CGRectMake(130, 0, 100, APP_ORDER_ITEM_HEADER_HEIGHT)];
    UILabel *pri = [[UILabel alloc] initWithFrame:CGRectMake(250, 0, 200, APP_ORDER_ITEM_HEADER_HEIGHT)];
    UILabel *tot = [[UILabel alloc] initWithFrame:CGRectMake(390, 0, 100, APP_ORDER_ITEM_HEADER_HEIGHT)];
    
    [qua setText:@"Quantity"];
    [cod setText:@"Code"];
    [pri setText:@"Product price"];
    [tot setText:@"Total"];
    
    UIView *hV = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, APP_ORDER_ITEM_HEADER_HEIGHT)];
    [hV setBackgroundColor:[UIColor whiteColor]];
    [hV addSubview:qua];
    [hV addSubview:cod];
    [hV addSubview:pri];
    [hV addSubview:tot];
    return hV;
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if(alertView.tag == 45){
        NSLog(@"button index pressed: %d", (int)buttonIndex);
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
    else if(alertView.tag == 87){
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
    else if(alertView.tag == 20){
        NSLog(@"button index pressed: %d", (int)buttonIndex);
        [self.navigationController popToRootViewControllerAnimated:YES];
    }

}

@end
